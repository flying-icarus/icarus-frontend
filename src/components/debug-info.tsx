import React from 'react'
import { connect } from 'react-redux'
import { IRootState } from '../models'


function DebugInfo(props: any) {
    let bbox = props.bbox.map(
        (num: number) => num.toPrecision(3)
    ).join(", ")

    if (props.error !== null) {
        console.log(props.error.stack)
    }

    return <div id="icarus-debug-info">
        <p>Number of objects: {props.numStates}</p>
        <p>Map view bounding box: {bbox}</p>
        {
            props.error !== null &&
            <p>Whoopsie: {props.error.toString()}</p>
        }
    </div>
}

const mapStateToProps = (state: IRootState) => {
    return {
        numStates: Object.keys(state.map.states).length,
        bbox: state.map.bbox,
        error: state.app.error,
    }
}

export default connect(mapStateToProps)(DebugInfo)
