import React from 'react'
import { connect } from 'react-redux'
import DatePicker from 'react-datepicker'
import * as actions from '../actions'
import * as models from '../models'

import 'react-datepicker/dist/react-datepicker.css'

interface PlaybackProps {
    playbackEnabled: boolean,
    playbackSpeed: number,
    playbackTimestamp: number,
    playbackLastRequestedTimestamp: number | null,
    setPlaybackEnabled: Function,
    setPlaybackDisabled: Function,
}

function Playback(props: PlaybackProps) {
    function onDateChange(newDate: Date) {
        props.setPlaybackEnabled(newDate.getTime(), props.playbackSpeed)
    }

    function onEnabledChange(ev: any) {
        if (ev.target.checked) {
            props.setPlaybackEnabled(
                props.playbackTimestamp,
                props.playbackSpeed,
            )
        }
        else {
            props.setPlaybackDisabled()
        }
    }

    function onSpeedChange(ev: any) {
        props.setPlaybackEnabled(props.playbackTimestamp, ev.target.value)
    }

    return <div id="icarus-playback">
        <span>Playback enabled:</span>
        <input
            type="checkbox"
            onChange={onEnabledChange}
            checked={props.playbackEnabled}
        />
        <DatePicker
            selected={new Date(props.playbackTimestamp)}
            onChange={onDateChange}
            allowSameDay={true}
            dateFormat="yyyy-MM-d HH:mm"
            timeFormat="HH:mm"
            showTimeSelect
        />
        <span>Speed:</span>
        <input type="number" value={props.playbackSpeed} onChange={onSpeedChange} />
        {
            props.playbackLastRequestedTimestamp !== null &&
            <span>Last requested at: {new Date(props.playbackLastRequestedTimestamp).toISOString()}</span>
        }
    </div>
}

function mapStateToProps(rootState: models.IRootState) {
    return {
        playbackEnabled: rootState.map.playbackEnabled,
        playbackSpeed: rootState.map.playbackSpeed || 1.0,
        playbackTimestamp: rootState.map.playbackTimestamp || Date.now(),
        playbackLastRequestedTimestamp: rootState.map.playbackLastRequestedTimestamp,
    }
}

function mapDispatchToProps(dispatch: Function) {
    return {
        setPlaybackEnabled: (timestamp: number, speed: number) => (
            dispatch(actions.playbackEnable(timestamp, speed))
        ),
        setPlaybackDisabled: () => {
            dispatch(actions.playbackDisable())
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Playback)
