import { connect } from 'react-redux'
import React from 'react'
import { BrowserRouter as Router, Route, NavLink } from 'react-router-dom'
import Container from 'react-bootstrap/Container'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Map from './map'
import { IRootState } from '../models'
import FlightInfo from './flight-info'
import DebugInfo from './debug-info'
import Playback from './playback'
import Search from './search'
import ObjectHistoryByICAO from './search/by-icao'
import ObjectHistoryByCallsign from './search/by-callsign'

import 'bootstrap/dist/css/bootstrap.css'
import './app.scss'


function Index(props: any) {
    let children = []
    if (props.showDebugInfo) {
        children.push(<DebugInfo />)
    }

    return (
        <div id="app">
            <FlightInfo />
            <Map id="map" />
            <Playback />
            {children}
        </div>
    )
}

function AppRouter() {
    return (
        <Router>
            <Container fluid={true} className="p-0" id="root-container">
                <Navbar bg="dark" expand="lg" variant="dark">
                    <Navbar.Brand as={NavLink} to="/">Flying Icarus</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                            <Nav.Link as={NavLink} to="/search">Search</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
                <Route path="/" exact component={Index} />
                <Route path="/search" component={Search} />
                <Route
                    path={`/objects/history/by-icao/:icao`}
                    component={ObjectHistoryByICAO} />
                <Route
                    path={`/objects/history/by-callsign/:callsign`}
                    component={ObjectHistoryByCallsign} />
            </Container>
        </Router>
    )
}

function mapStateToProps(state: IRootState) {
    return {
        showDebugInfo: state.app.showDebugInfo,
    }
}

export default connect(mapStateToProps)(AppRouter)
