import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import * as models from '../models'

interface FlightInfoProps {
    state: models.ObjectState | null
    history: models.FlightHistory | null
}

function FlightInfo(props: FlightInfoProps) {
    if ((props.state === null) || (props.history === null)) {
        return <div id="icarus-flight-info" className="icarus-flight-info-inactive"></div>
    }

    const icaoHex: string = props.state.icao.toString(16)
    let picture: models.Picture | null
    if (props.history.pictures.length === 0) {
	picture = null
    }
    else {
	picture = props.history.pictures[0]
    }

    return <div id="icarus-flight-info" className="icarus-flight-info-active">
	{picture !== null ?
	 <div className="icarus-flight-info-picture">
	     <a href={picture.picturePageUrl} target="_blank" rel="noopener noreferrer">
		 <img src={picture!.thumbnailUrl} alt="airplane" />
	     </a>
	 </div>
	: null}
	<div className="row no-gutters icarus-flight-info-row">
	    <div className="col-1 panel-icon">
		<svg viewBox="0 0 40 40">
		    <path d="M11.9,39.1l-0.1-1.3c0,0-0.2-1.9-0.2-2.3c-0.1-0.6-0.1-1,5.1-5.3c-0.1-1.4-0.2-5.6-0.2-7.7
			c-1.9,0.5-6.8,2.9-11,5.1l-1.2,0.6L4,26.8c0-0.1-0.3-1.5-0.4-2.9C3.5,23.3,3.4,22,16.3,12.7c0-1.4-0.1-5.3,0-7
			c0.1-2.7,3.2-4.2,3.3-4.3L20,1.2l0.5,0.2c0.3,0.2,3.2,1.8,3.3,4.3l0,0c0.1,1.8,0,5.6,0,7C36.6,22,36.5,23.3,36.4,23.9
			c-0.1,1.4-0.4,2.9-0.4,2.9l-0.3,1.3l-1.2-0.6c-4.1-2.2-9.1-4.6-11-5.1c0,2.1-0.1,6.3-0.2,7.7c5.2,4.3,5.1,4.7,5.1,5.3
			c-0.1,0.4-0.2,2.3-0.2,2.3L28.1,39l-1.2-0.4c0,0-6-1.9-7.1-2.3c-0.9,0.1-4.1,1.2-6.7,2.2L11.9,39.1z M20,34.3c0.2,0,0.2,0,6.4,2
			c0-0.3,0.1-0.6,0.1-0.8c-0.6-0.7-2.8-2.6-4.7-4.2L21.4,31l0-0.5c0.1-2.5,0.3-7.6,0.2-8.9c0-0.5,0.2-0.9,0.5-1.1
			c0.5-0.3,1.6-1,12.2,4.6c0.1-0.4,0.1-0.8,0.2-1.2c-0.8-1.2-6.8-5.9-12.3-9.8l-0.4-0.3l0-0.5c0-0.1,0.2-5.4,0.1-7.4l0,0
			c0-1-1.1-1.9-1.8-2.4c-0.7,0.5-1.7,1.3-1.8,2.3c-0.1,2.1,0.1,7.4,0.1,7.4l0,0.5l-0.4,0.3C12.4,18,6.4,22.6,5.6,23.9
			c0,0.4,0.1,0.8,0.2,1.2c10.6-5.6,11.7-4.9,12.2-4.6c0.4,0.2,0.6,0.7,0.5,1.1c-0.1,1.3,0.1,6.4,0.2,8.9l0,0.5l-0.4,0.3
			c-1.9,1.6-4.1,3.5-4.7,4.2c0,0.2,0,0.5,0.1,0.7C15.6,35.5,18.8,34.3,20,34.3z M13.5,35.2C13.5,35.2,13.5,35.2,13.5,35.2
			C13.5,35.2,13.5,35.2,13.5,35.2z M5.5,24L5.5,24L5.5,24z"></path>
		</svg>
	    </div>
	    <div className="col-md">
		<div className="row no-gutters">
		    <div className="col">
			<div className="panel-data-title">Callsign</div>
			<div className="panel-data">
			    {props.state.callsign !== null ?
			     <Link to={"/objects/history/by-callsign/" + props.state.callsign}>
				 {props.state.callsign}
			     </Link>
			    :
			     "Unknown"
			    }
			</div>
		    </div>
		    <div className="col">
			<div className="panel-data-title">ICAO Code</div>
			<div className="panel-data">
			    <Link to={"/objects/history/by-icao/" + icaoHex}>
				{icaoHex}
			    </Link>
			</div>
		    </div>
		</div>
		<div className="row no-gutters">
		    <div className="col-12">
			<div className="panel-data-title">Squawk</div>
			<div className="panel-data">{props.state.squawk}</div>
		    </div>
		</div>
	    </div>
	</div>
        <div className="row no-gutters icarus-flight-info-row">
	    <div className="col-1 panel-icon">
		<svg className="mx-auto d-block" viewBox="0 0 40 40">
		    <path d="M38.37,11.73c-1.41-0.93-4.59-1.21-8.6-1.46c-1.33-0.09-2.98-0.19-3.45-0.33c0,0-0.01-0.01-0.02-0.01
        		c-0.24-0.17-0.98-0.71-1.99-1.45c-8.05-5.91-9.66-6.97-10.31-7.12c-0.54-0.08-1.89-0.21-2.65,0.51c-0.8,0.8-0.48,1.76-0.33,2.19
        		c0.1,0.24,0.29,0.5,2.38,3.17c0.75,0.95,1.77,2.25,2.35,3.03c-1.92,0.11-5.76,0.12-6.41,0.01c0,0-0.01,0-0.01,0
        		c-0.56-0.25-3.72-2.28-6.51-4.13C2.67,6.04,2.48,5.98,2.29,5.98h-1.1c-0.34,0-0.66,0.18-0.84,0.46C0.16,6.73,0.13,7.1,0.28,7.41
        		c4.5,9.54,4.56,9.57,4.9,9.78c0,0,0,0,0,0c0.33,0.2,8.56,0.25,16.48,0.25c7.41,0,14.55-0.04,14.69-0.05c0.05,0,0.1-0.01,0.14-0.02
        		c2.64-0.5,3.21-1.89,3.32-2.68C39.98,13.46,39.21,12.2,38.37,11.73z M36.21,15.39c-2.33,0.07-26.03,0.04-29.9-0.04
        		c-0.55-1.1-1.95-4.02-3.19-6.62c3.93,2.59,5.1,3.24,5.52,3.41c0.18,0.09,0.62,0.26,4.5,0.2c4.11-0.05,4.35-0.26,4.63-0.49
        		c0.05-0.04,0.1-0.09,0.14-0.14c0.42-0.52,0.27-1.07,0.19-1.4l-0.03-0.11c-0.08-0.33-0.2-0.52-3.09-4.21
        		c-0.8-1.02-1.77-2.26-2.08-2.69c0.19-0.02,0.46-0.02,0.7,0.01c1.04,0.53,6.97,4.88,9.54,6.77c0.93,0.68,1.63,1.2,1.94,1.42
        		c0.51,0.47,1.53,0.56,4.56,0.75c2.47,0.16,6.61,0.43,7.64,1.15c0.02,0.01,0.11,0.06,0.13,0.08c0.16,0.09,0.49,0.57,0.42,0.96
        		C37.76,14.85,37.16,15.2,36.21,15.39z"></path>
        	    <path d="M23.65,25.89c0.2,0.19,0.45,0.29,0.7,0.29c0.26,0,0.51-0.1,0.71-0.3c0.39-0.39,0.39-1.03-0.01-1.41l-4.26-4.22
        		c-0.38-0.38-1-0.39-1.39-0.01l-4.42,4.22c-0.4,0.38-0.41,1.01-0.03,1.41c0.38,0.4,1.01,0.41,1.41,0.03l2.65-2.53V35.4l-2.63-2.61
        		c-0.39-0.39-1.03-0.39-1.41,0.01c-0.39,0.39-0.39,1.03,0.01,1.41l4.26,4.22c0.19,0.19,0.45,0.29,0.7,0.29
        		c0.25,0,0.5-0.09,0.69-0.28l4.42-4.22c0.4-0.38,0.41-1.01,0.03-1.41c-0.38-0.4-1.01-0.41-1.41-0.03l-2.65,2.53V23.28L23.65,25.89z"></path>
		</svg>
	    </div>
	    <div className="col-md">
		<div className="row no-gutters">
		    <div className="col">
			<div className="panel-data-title">Barometric Altitude</div>
			<div className="panel-data">{props.state.baroAltitude} ft</div>
		    </div>
		    <div className="col">
			<div className="panel-data-title">Vertical Speed</div>
			<div className="panel-data">{props.state.verticalRate} fpm</div>
		    </div>
		</div>
		<div className="row no-gutters">
		    <div className="col">
			<div className="panel-data-title">GPS Altitude</div>
			<div className="panel-data">{props.state.geoAltitude} ft</div>
		    </div>
		    <div className="col">
			<div className="panel-data-title">Heading</div>
			<div className="panel-data">
			    {props.state.heading !== null ? Math.round(props.state.heading) + "°" : "N/A"}
			</div>
		    </div>
		</div>
	    </div>
	</div>
	<div className="row no-gutters icarus-flight-info-row">
	    <div className="col-1 panel-icon">
		<svg viewBox="0 0 40 40">
		    <path d="M27.7,1.7C27.7,1.7,27.7,1.7,27.7,1.7C27.6,1.7,27.6,1.7,27.7,1.7c-2.4-1-5-1.6-7.7-1.6C9.1,0.1,0.1,9,0.1,20 c0,10.9,8.9,19.8,19.8,19.8c10.9,0,19.8-8.9,19.8-19.8C39.8,11.7,34.8,4.7,27.7,1.7z M33.6,31.5c0-0.1-0.1-0.1-0.1-0.2L31.2,29 c-0.4-0.4-1-0.4-1.4,0c-0.4,0.4-0.4,1,0,1.4l2.2,2.3c0.1,0.1,0.2,0.1,0.3,0.2c-1.3,1.3-2.8,2.3-4.5,3.1l-0.5-1.2 c-0.2-0.5-0.8-0.7-1.3-0.5c-0.5,0.2-0.7,0.8-0.5,1.3l0.5,1.1c-1.6,0.6-3.2,0.9-4.9,1v-3.1c0-0.6-0.4-1-1-1s-1,0.4-1,1v3.1 c-1.8-0.1-3.6-0.5-5.2-1.1l0.5-1c0.3-0.5,0.1-1.1-0.4-1.3c-0.5-0.3-1.1-0.1-1.3,0.4l-0.6,1.1c-1.5-0.8-2.8-1.7-4.1-2.8c0,0,0,0,0,0 l2.2-2.3c0.4-0.4,0.4-1,0-1.4c-0.4-0.4-1-0.4-1.4,0l-2.2,2.3c-1-1.2-1.8-2.5-2.5-3.9l0.9-0.4C5.4,27,5.6,26.5,5.4,26 c-0.2-0.5-0.8-0.7-1.3-0.5l-0.9,0.4c-0.6-1.7-0.9-3.4-1-5.3h3.3c0.6,0,1-0.4,1-1s-0.4-1-1-1H2.2c0.1-1.7,0.5-3.4,1.1-5L4.4,14 c0.1,0,0.2,0.1,0.4,0.1c0.4,0,0.8-0.2,0.9-0.6c0.2-0.5-0.1-1.1-0.6-1.3l-1-0.4C4.9,10.3,5.9,9,7,7.8l1.9,1.9C9,10,9.3,10.1,9.5,10.1 c0.2,0,0.5-0.1,0.7-0.3c0.4-0.4,0.4-1,0-1.4L8.4,6.4C9.5,5.5,10.7,4.7,12,4l0.4,1c0.2,0.4,0.5,0.6,0.9,0.6c0.1,0,0.3,0,0.4-0.1 c0.5-0.2,0.8-0.8,0.5-1.3l-0.4-1c1.6-0.6,3.4-1,5.2-1.1v2.6c0,0.6,0.4,1,1,1s1-0.4,1-1V2.2c1.7,0.1,3.3,0.4,4.8,1L25.5,4 c-0.2,0.5,0,1.1,0.5,1.3c0.1,0.1,0.3,0.1,0.4,0.1c0.4,0,0.7-0.2,0.9-0.6l0.4-0.9c1.3,0.6,2.6,1.4,3.7,2.4l-2,2.1 c-0.4,0.4-0.4,1,0,1.4c0.2,0.2,0.4,0.3,0.7,0.3c0.3,0,0.5-0.1,0.7-0.3l2-2.1c1.2,1.2,2.2,2.6,3,4.2l-1,0.5c-0.5,0.3-0.7,0.9-0.4,1.4 c0.2,0.3,0.5,0.5,0.9,0.5c0.2,0,0.3,0,0.5-0.1l0.8-0.4c0.6,1.5,1,3.2,1.1,4.9h-3.1c-0.6,0-1,0.4-1,1s0.4,1,1,1h3.2 c-0.1,1.7-0.3,3.3-0.9,4.9l-1.2-0.5c-0.5-0.2-1.1,0-1.3,0.5c-0.2,0.5,0,1.1,0.5,1.3l1.3,0.6C35.5,28.9,34.6,30.3,33.6,31.5z"></path>
		    <path d="M24.7,8.7c-0.5-0.2-1.1,0-1.3,0.5l-3.6,8.5c-1.1,0.1-2,1-2,2.2c0,0.5,0.2,1,0.5,1.4L17.6,23 c-0.2,0.5,0,1.1,0.5,1.3c0.1,0.1,0.3,0.1,0.4,0.1c0.4,0,0.8-0.2,0.9-0.6l0.7-1.6c1.1-0.1,2.1-1,2.1-2.2c0-0.5-0.2-1-0.5-1.4l3.6-8.5 C25.5,9.5,25.2,9,24.7,8.7z"></path>
		    <line x1="13.8" y1="28" x2="25.9" y2="28"></line>
		</svg>
	    </div>
	    <div className="col-md">
		<div className="row no-gutters">
		    <div className="col-12">
			<div className="panel-data-title">Ground Speed</div>
			<div className="panel-data">{props.state.velocity} kts</div>
		    </div>
		</div>
	    </div>
	</div>
    </div>
}

function mapStateToProps(rootState: models.IRootState): FlightInfoProps {
    let selectedObject = rootState.map.selectedObject
    let objectState: FlightInfoProps["state"]
    if (selectedObject === null) {
        objectState = null
    }
    else {
        objectState = rootState.map.states[selectedObject]
    }

    return {
        state: objectState,
	history: rootState.map.history
    }
}

export default connect(mapStateToProps)(FlightInfo)
