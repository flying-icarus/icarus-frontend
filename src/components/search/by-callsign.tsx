import React, { useState } from 'react'
import { connect } from 'react-redux'
import Table from 'react-bootstrap/Table'
import * as models from '../../models'
import { requestObjectHistory } from '../../actions'

interface IProps {
    callsign: string,
    loading: boolean,
    flights: models.Flight[],
    requestObjectHistory: Function,
}

function HistoryByCallsign(props: IProps) {
    const [initialized, setInitialized] = useState(false)
    if (!initialized) {
        props.requestObjectHistory(props.callsign)
        setInitialized(true)
        return null
    }

    if (props.loading) {
        return <div>loading</div>
    }

    let rows = []
    for (const flight of props.flights) {
        rows.push(
            <tr key={flight.id}>
                <td>{flight.first_seen_at}</td>
                <td>{flight.last_seen_at}</td>
            </tr>
        )
    }

    return (
        <div>
            <h2>Flight history for {props.callsign}</h2>
            <Table>
                <thead>
                    <tr>
                        <th>Start time</th>
                        <th>End time</th>
                    </tr>
                </thead>
                <tbody>
                    {rows}
                </tbody>
            </Table>
        </div>
    )
}

function mapStateToProps(state: models.IRootState, ownProps: any): any {
    return {
        callsign: ownProps.match.params.callsign,
        loading: state.history.loading,
        flights: state.history.flights,
    }
}

function mapDispatchToProps(dispatch: Function) {
    return {
        requestObjectHistory: (callsign: string) => (
            dispatch(requestObjectHistory({ callsign }))
        )
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(HistoryByCallsign)
