import React, { useState } from 'react'
import Form from 'react-bootstrap/Form'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Container from 'react-bootstrap/Container'

export default function Search(props: any) {
    const [icao, setIcao] = useState('')
    const [reg, setReg] = useState('')
    const [callsign, setCallsign] = useState('')

    function redirect(path: string) {
        return (ev: any) => {
            ev.preventDefault()
            props.history.push(path)
            return false
        }
    }

    function setValue(setFn: Function) {
        return (ev: any) => {
            setFn(ev.currentTarget.value)
        }
    }

    return (
        <Container>
            <Row>
                <Col>
                    <Form onSubmit={redirect("/objects/history/by-icao/" + icao)}>
                        <Form.Control
                            type="text"
                            placeholder="Search aircraft by icao hex ID"
                            onChange={setValue(setIcao)}
                            value={icao}
                        />
                    </Form>
                </Col>
                <Col>
                    <Form>
                        <Form.Control
                            type="text"
                            placeholder="Search aircraft by registration"
                            onChange={setValue(setReg)}
                            value={reg}
                        />
                    </Form>
                </Col>
                <Col>
                    <Form onSubmit={redirect("/objects/history/by-callsign/" + callsign)}>
                        <Form.Control
                            type="text"
                            placeholder="Search aircraft by callsign"
                            onChange={setValue(setCallsign)}
                            value={callsign}
                        />
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}
