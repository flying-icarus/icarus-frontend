import React, { useState } from 'react'
import { connect } from 'react-redux'
import Table from 'react-bootstrap/Table'
import * as models from '../../models'
import { requestObjectHistory } from '../../actions'

interface IProps {
    icaoHex: string,
    loading: boolean,
    flights: models.Flight[],
    requestObjectHistory: Function,
}

function HistoryByICAO(props: IProps) {
    const [initialized, setInitialized] = useState(false)
    if (!initialized) {
        props.requestObjectHistory(parseInt(props.icaoHex, 16))
        setInitialized(true)
        return null
    }

    if (props.loading) {
        return <div>loading</div>
    }

    let rows = []
    for (const flight of props.flights) {
        rows.push(
            <tr key={flight.id}>
                <td>{flight.first_seen_at}</td>
                <td>{flight.last_seen_at}</td>
            </tr>
        )
    }

    return (
        <div>
            <h2>Flight history for {props.icaoHex}</h2>
            <Table>
                <thead>
                    <tr>
                        <th>Start time</th>
                        <th>End time</th>
                    </tr>
                </thead>
                <tbody>
                    {rows}
                </tbody>
            </Table>
        </div>
    )
}

function mapStateToProps(state: models.IRootState, ownProps: any): any {
    return {
        icaoHex: ownProps.match.params.icao,
        loading: state.history.loading,
        flights: state.history.flights,
    }
}

function mapDispatchToProps(dispatch: Function) {
    return {
        requestObjectHistory: (icao: number) => (
            dispatch(requestObjectHistory({ icao }))
        )
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(HistoryByICAO)
