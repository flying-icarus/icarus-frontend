import { connect } from 'react-redux'
import React from 'react'
import L from 'leaflet'
import { MapOverlays } from '../objects-overlay'
import * as actions from '../actions'
import * as models from '../models'
import config from '../config'


class Map extends React.Component<any, any> {
    map: L.Map | null
    tileLayer: L.Layer
    mapOverlays: MapOverlays | null
    _drawFakeTimer: number | null
    _drawFakeSpeed: number | null

    constructor(props: any) {
        super(props)
        this._drawFakeTimer = null
        this._drawFakeSpeed = null
        this.map = null
        this.mapOverlays = null
        this.tileLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 18,
        })
    }

    public componentDidMount() {
        this.mapOverlays = new MapOverlays()

        this.mapOverlays.on(
            'mouseenter-object',
            this._onMouseEnterObject.bind(this) as unknown as any,
        )

        this.mapOverlays.on(
            'mouseleave-object',
            this._onMouseLeaveObject.bind(this) as unknown as any,
        )

        this.mapOverlays.on(
            'object-select',
            this._onObjectSelect.bind(this) as unknown as any,
        )

        this.mapOverlays.on(
            'object-deselect',
            this._onObjectDeselect.bind(this) as unknown as any,
        )

        // Can't get the canvas transformation to play nicely with the map zoom
        // animation.
        this.map = L.map(this.props.id, {
            zoomAnimation: false,
            zoomControl: false,
        })

        L.control.zoom({ position: 'topright' }).addTo(this.map)
        L.control.scale().addTo(this.map)
        this.map.addLayer(this.tileLayer)
        this.map.addLayer(this.mapOverlays)
        this.map.on('moveend', this._onMapMoveEnd.bind(this))
        let bbox = this.props.bbox
        this.map.fitBounds([
            [bbox[0], bbox[1]],
            [bbox[2], bbox[3]]
        ])
    }

    // LeafletEvent definition buggy.
    _onMouseEnterObject(ev: CustomEvent) {
        let newMarker = this.mapOverlays!.markers[ev.detail.icao]
        this.mapOverlays!.styleObjectHighlighted(newMarker)
        this.props.onObjectMouseEnter(newMarker.state.icao)

        L.DomUtil.addClass(
            this.map!.getContainer(),
            'cursor-pointer',
        )
    }

    _onMouseLeaveObject(ev: CustomEvent) {
        let oldMarker = this.mapOverlays!.markers[ev.detail.icao]
        this.mapOverlays!.styleObjectNormal(oldMarker)
        this.props.onObjectMouseLeave()

        L.DomUtil.removeClass(
            this.map!.getContainer(),
            'cursor-pointer',
        )
    }

    _resetDrawFakeTimer() {
        if (this._drawFakeSpeed !== this.props.speed) {
            if (this._drawFakeTimer !== null) {
                clearTimeout(this._drawFakeTimer!)
            }

            this._drawFakeTimer = window.setInterval(() => {
                window.requestAnimationFrame(() => {
                    this.mapOverlays!.drawFake(this.props.speed)
                })
            }, config.MAP_FAKE_UPDATE_EVERY_MS / this.props.speed)
            this._drawFakeSpeed = this.props.speed
        }
    }

    _onMapMoveEnd() {
        let bbox_map = this.map!.getBounds()
        // If map is zoomed out too much it will return unbound degrees.
        this.props.onBBoxChange([
            Math.max(bbox_map.getSouth(), -90),
            Math.max(bbox_map.getWest(), -180),
            Math.min(bbox_map.getNorth(), 90),
            Math.min(bbox_map.getEast(), 180),
        ])
    }

    _onObjectSelect(ev: CustomEvent) {
        this.props.onObjectSelected(ev.detail.icao)
    }

    _onObjectDeselect() {
        this.props.onObjectDeselect()
    }

    render() {
        return <div id={this.props.id}></div>
    }

    componentDidUpdate(prevProps: any) {
        if (this.map === null) {
            return
        }

        this._resetDrawFakeTimer()

        // TODO: refactor trail to be null on unset.
        if (this.props.trail === null) {
            this.mapOverlays!.clearTrail()
        } else {
            this.mapOverlays!.setTrail(this.props.trail)
        }

        if (prevProps.selectedObject !== null) {
            let oldMarker = this.mapOverlays!.markers[prevProps.selectedObject]
            // Workaround for react-router recreating us when switching routes.
            // TODO: update state when this happens to reflect reality.
            if (oldMarker !== undefined) {
                this.mapOverlays!.styleObjectNormal(oldMarker)
            }

        }

        if (this.props.selectedObject !== null) {
            let newMarker = this.mapOverlays!.markers[this.props.selectedObject]
            // Workaround for react-router recreating us when switching routes.
            // TODO: update state when this happens to reflect reality.
            if (newMarker !== undefined) {
                this.mapOverlays!.styleObjectHighlighted(newMarker)
            }
        }

        if (this.props.states !== prevProps.states) {
            this.mapOverlays!.setStates(this.props.states)
            window.requestAnimationFrame(this.mapOverlays!.draw.bind(this.mapOverlays))
        }
    }
}



function mapStateToProps(state: models.IRootState) {
    return {
        states: state.map.states,
        trail: state.map.history !== null ? state.map.history.states : null,
        bbox: state.map.bbox,
        speed: state.map.playbackSpeed || 1.0,
        mouseOverObject: state.map.mouseOverObject,
        selectedObject: state.map.selectedObject,
    }
}

function mapDispatchToProps(dispatch: Function) {
    return {
        onObjectSelected: (icao: number) => (
            dispatch(actions.setSelectedObject(icao))
        ),
        onBBoxChange: (bbox: models.BoundingBox) => (
            dispatch(actions.setViewBBox(bbox))
        ),
        onObjectDeselect: () => {
            dispatch({ type: actions.ACTION_TYPES.MAP_OBJECT_DESELECTED })
        },
        onObjectMouseEnter: (icao: number) => (
            dispatch(actions.setMouseOverObject(icao))
        ),
        onObjectMouseLeave: () => (
            dispatch(actions.unsetMouseOverObject())
        )
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Map)
