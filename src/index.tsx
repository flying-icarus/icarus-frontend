import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { composeWithDevTools } from 'redux-devtools-extension'
import rootReducer from './reducers'
import App from './components/app'
import './index.css'
import * as serviceWorker from './serviceWorker'
import rootSagas from './sagas'


const sagaMiddleware = createSagaMiddleware()
const composeEnhancers = composeWithDevTools({ trace: true, traceLimit: 25 })
const store = createStore(
    rootReducer,
    composeEnhancers(
        applyMiddleware(sagaMiddleware),
    ),
)

sagaMiddleware.run(rootSagas)

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister()
