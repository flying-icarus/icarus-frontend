import * as models_pbuf from './models_pbuf'
import * as models from "./models"
import config from './config'

let HEADER_ACCEPTS_PROTOBUF = {
    'Accept': 'application/x-protobuf'
}

export function getObjectStates(timestamp: number, bbox: models.BoundingBox): Promise<models.ObjectStatesMap> {
    let timestamp_s = Math.floor(timestamp / 1000)
    let bbox_str = bbox.join(",")
    let url = config.API_BASE_URL + '/states?bbox=' + encodeURIComponent(bbox_str) + '&timestamp=' + timestamp_s
    return fetch(new Request(url), {
        headers: HEADER_ACCEPTS_PROTOBUF
    })
        .then((resp) => resp.arrayBuffer())
        .then((data: ArrayBuffer) => {
            let statesMap = models_pbuf.ObjectStatesMap.decode(new Uint8Array(data))
            return statesMap.states as models.ObjectStatesMap
        })
}

export function getObjectTrail(icao: number): Promise<models.FlightHistory> {
    let url = config.API_BASE_URL + '/active-flights/' + icao
    return fetch(new Request(url), {
        headers: HEADER_ACCEPTS_PROTOBUF,
    })
        .then((resp) => resp.arrayBuffer())
        .then((data: ArrayBuffer) => {
            let flightHistory = models_pbuf.FlightHistory.decode(new Uint8Array(data))
            return flightHistory as unknown as models.FlightHistory
        })
}

export interface IGetObjectHistoryIn {
    icao?: number,
    registration?: string,
    callsign?: string,
}

export function getObjectHistory(filter: IGetObjectHistoryIn): Promise<models.ObjectHistory> {
    let url = config.API_BASE_URL + '/aircraft-history?'
    if (filter.icao !== undefined) {
        url += 'icao=' + filter.icao
    }

    else if (filter.registration !== undefined) {
        url += 'registration=' + filter.registration
    }
    else if (filter.callsign !== undefined) {
        url += 'callsign=' + filter.callsign
    }

    return fetch(url)
        .then((resp) => resp.json())
        .then((history: models.ObjectHistory) => history)
}
