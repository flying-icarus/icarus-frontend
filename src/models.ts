import { IState as IAppState } from './reducers/app'
import { IState as IMapState } from './reducers/map'
import { IState as IHistoryState } from './reducers/history'

export interface IRootState {
    app: IAppState,
    map: IMapState,
    history: IHistoryState,
}

export interface RootError {
    message: string
}

// south, west, north, east
export type BoundingBox = [number, number, number, number]

export interface ObjectStatesMap {
    [icao: number]: ObjectState
}

export type ObjectStatesList = ObjectState[]

export interface ObjectState {
    icao: number
    callsign: string | null
    lat: number
    lon: number
    geoAltitude: number | null
    baroAltitude: number | null
    velocity: number | null
    heading: number | null
    verticalRate: number | null
    squawk: number | null
    reportedAt: number | null
}

export interface Flight {
    id: string,
    first_seen_at: number
    last_seen_at: number | null
}

export interface ObjectHistory {
    flights: Flight[]
}

export interface Picture {
    thumbnailUrl: string
    picturePageUrl: string
    photographerFullName: string
}

export interface FlightHistory {
    states: ObjectStatesList
    pictures: Picture[]
}
