import L from 'leaflet'
import Konva from 'konva'
import * as THREE from 'three'
import * as models from './models'

import debounce from 'lodash/debounce'
import 'leaflet/dist/leaflet.css'

interface MapObjectsMap {
    [icao: number]: MapObject
}


export type onMouseMoveT = (ev: MouseEvent) => void
export type onObjectMouseEnterT = (mapObject: MapObject) => void
export type onObjectMouseLeaveT = (mapObject: MapObject) => void

export class MapOverlays extends L.Layer {
    markers: MapObjectsMap

    stage: Konva.Stage | null
    trailsLayer: Konva.FastLayer
    databoxLayer: Konva.FastLayer
    trailStates: models.ObjectStatesList | null
    lastDrawFakeAt: number
    objectsOverlay: ObjectsOverlay | null
    dragging = false

    constructor() {
        super()
        this.objectsOverlay = null
        this.stage = null
        this.markers = {}
        this.trailsLayer = new Konva.FastLayer()
        this.databoxLayer = new Konva.FastLayer()
        this.lastDrawFakeAt = 0
        this.trailStates = null
    }

    setStates(newStates: models.ObjectStatesMap) {
        for (let icao in newStates) {
            const newState = newStates[icao]
            if (!(icao in this.markers)) {
                let marker = new MapObject(newState)
                this.markers[icao] = marker
            }

            let marker = this.markers[icao]

            if (marker.state !== newState) {
                marker.setState(newState)
            }
        }

        // TODO: stupid.
        for (let icao in this.markers) {
            if (!(icao in newStates)) {
                delete this.markers[icao]
            }
        }

        this.objectsOverlay!.setObjects(this.markers)
    }

    onAdd(map: L.Map): this {
        let container = L.DomUtil.create("div", "leaflet-layer") as HTMLDivElement
        let mapSize = map.getSize()
        this.stage = new Konva.Stage({
            container: container,
            width: mapSize.x,
            height: mapSize.y,
        })

        this.stage.add(this.trailsLayer)
        this.stage.add(this.databoxLayer)
        map.getPanes().overlayPane.appendChild(container)

        // Place inside the Konva stage, because I cannot get them to both show
        // otherwise.
        let objectsOverlayCanvas = L.DomUtil.create(
            "canvas",
            "objects-overlay",
            // Konva stage div:
            container.children[0] as HTMLElement
        ) as HTMLCanvasElement

        objectsOverlayCanvas.width = mapSize.x
        objectsOverlayCanvas.height = mapSize.y
        objectsOverlayCanvas.style.position = 'relative'
        objectsOverlayCanvas.style.width = `${mapSize.x}px`
        objectsOverlayCanvas.style.height = `${mapSize.y}px`

        this.objectsOverlay = new ObjectsOverlay(
            objectsOverlayCanvas,
            map.latLngToContainerPoint.bind(map),
            map.getZoom(),
            this,
        )

        for (const evName of [
            'mouseenter-object',
            'mouseleave-object',
            'object-select',
        ]) {
            this.objectsOverlay.addEventListener(evName, this.fire.bind(this, evName))
        }

        // For yet unknown reasons the stage "click" event fires after panning the map.
        // As a workaround, set a flag when the panning begins, so we know when
        // the click comes to ignore it.
        map.on("dragstart", () => this.dragging = true)
        objectsOverlayCanvas.addEventListener('click', (ev) => {
            if (this.dragging) {
                this.dragging = false
                ev.preventDefault()
            }
        })

        let animated = map.options.zoomAnimation && L.Browser.any3d
        L.DomUtil.addClass(container, 'leaflet-zoom-' + (animated ? 'animated' : 'hide'))

        if (animated === true) {
            map.on("zoomanim", (ev: any) => {
                var scale = map.getZoomScale(ev.zoom, map.getZoom())
                var offset = map._latLngBoundsToNewLayerBounds(map.getBounds(), ev.zoom, ev.center).min
                L.DomUtil.setTransform(container, offset, scale)
            })
        }

        // Cannot set L.ZoomAnimEvent here because the definitions are too
        /// generic.
        map.on("resize", (ev: any) => {
            this.stage!.size({
                width: ev.newSize.x,
                height: ev.newSize.y
            })
        })

        map.on("zoomend", () => {
            this.objectsOverlay!.updateMapZoom(map.getZoom())
        })

        return this
    }

    setTrail(states: models.ObjectStatesList) {
        this.trailStates = states
        this.drawTrail()
    }

    drawTrail() {
        if (this.trailStates === null) {
            throw new Error('cant draw null trails')
        }

        this.trailsLayer.removeChildren()
        let trailLine = new TrailLine(this._map, this.trailStates)
        this.trailsLayer.add(trailLine)
        this.trailsLayer.draw()
    }

    clearTrail() {
        this.trailStates = null
        this.trailsLayer.removeChildren()
        this.trailsLayer.clear()
    }

    draw() {
        this.lastDrawFakeAt = Date.now()
        let topLeft = this._map.containerPointToLayerPoint([0, 0])

        // Unset transformation
        L.DomUtil.setTransform(this.stage!.container(), topLeft, 1)
        if (this.trailStates !== null) {
            this.drawTrail()
        }

        this.databoxLayer.removeChildren()
        this.databoxLayer.clear()

        let zoom = this._map.getZoom()
        if (zoom > 7) {
            for (var icao in this.markers) {
                let marker = this.markers[icao]
                let state = marker.getEffectiveState()
                let databox = new DataboxShape(marker.getEffectiveState())
                this.databoxLayer.add(databox)
                databox.computeWidth()
                let pos = this._map.latLngToContainerPoint([
                    state.lat,
                    state.lon,
                ])

                pos.x -= databox.width() / 2
                if (state.heading !== null) {
                    const offsetX = this.objectsOverlay!.getTipToCenterOffsetX(state.heading!)
                    pos.x += offsetX / 2
                }

                pos.y -= 30

                databox.position(pos)

            }

            this.databoxLayer.draw()
        }

        this.objectsOverlay!.render()
    }

    drawFake(speed: number) {
        let timeElapsed = (Date.now() - this.lastDrawFakeAt) * speed
        for (var icao in this.markers) {
            let marker = this.markers[icao]
            marker.moveFake(timeElapsed)
        }

        this.objectsOverlay!.setObjects(this.markers)
        this.draw()
    }

    public styleObjectNormal(mapObject: MapObject) {
        this.objectsOverlay!.styleObjectNormal(mapObject)
    }

    public styleObjectHighlighted(mapObject: MapObject) {
        this.objectsOverlay!.styleObjectHighlighted(mapObject)
    }
}

export class MapObject {
    state: models.ObjectState
    fakeState: models.ObjectState | null

    constructor(state: models.ObjectState) {
        this.state = state
        this.fakeState = null
    }

    setState(state: models.ObjectState) {
        this.state = state
        this.fakeState = null
    }

    moveFake(ms: number) {
        // velocity is m/s
        if ((this.state.velocity === null) || (this.state.heading === null)) {
            return
        }

        if (this.fakeState === null) {
            // Copy because we'll edit it when faking.
            this.fakeState = Object.assign({}, this.state)
        }

        // TODO: There's something wrong with this cause it makes them move
        // faster than reality. For now workaround by reducing the velocity.
        let distance = (this.state.velocity) * 0.7 * ms / 1000
        let heading_rad = Math.PI / (180 / this.state.heading)
        let cos_h = Math.cos(heading_rad)
        let sin_h = Math.sin(heading_rad)
        let cos_lat = Math.cos(Math.PI / (180 / this.state.lat))
        this.fakeState.lat += distance * cos_h / 111111
        this.fakeState.lon += distance * sin_h / cos_lat / 111111
    }

    getEffectiveState() {
        if (this.fakeState !== null) {
            return this.fakeState
        }
        else {
            return this.state
        }
    }
}


class DataboxShape extends Konva.Shape {
    static _height = 11
    static _font = '11px mono'

    state: models.ObjectState

    // TODO: Konva.Context missing from the type definitions.
    static _sceneFunc(context: any, shape: any) {
        let ctx = context.getCanvas().getContext()
        ctx.font = DataboxShape._font
        ctx.textAlign = 'center'
        ctx.globalAlpha = 0.5
        ctx.fillStyle = 'black'

        let callsign = shape.getCallsignText()
        let width = shape.width()
        ctx.fillRect(0, 0, width, DataboxShape._height)
        ctx.globalAlpha = 1
        ctx.fillStyle = 'white'
        ctx.fillText(callsign, width / 2, DataboxShape._height - 2)
    }

    constructor(state: models.ObjectState) {
        super({
            sceneFunc: DataboxShape._sceneFunc,
            perfectDrawEnabled: false,
            listening: false,
        })

        this.state = state
    }

    protected getCallsignText() {
        return this.state.callsign || "NO CLSGN"
    }

    public computeWidth() {
        let ctx = this.getCanvas().getContext()
        ctx.font = DataboxShape._font
        this.width(ctx.measureText(this.getCallsignText()).width)
    }
}

class TrailLine extends Konva.Shape {
    states: models.ObjectState[]
    map: L.Map

    // TODO: Konva.Context missing from the type definitions.
    static _sceneFunc(context: any, shape: any) {
        let ctx = context.getCanvas().getContext()
        let prevState: models.ObjectState | null = null
        ctx.lineWidth = 2.1
        for (let state of shape.states) {
            ctx.beginPath()
            let point = shape.map.latLngToContainerPoint([state.lat, state.lon])
            let strokeStyle = TrailLine.stateToColor(state)
            ctx.strokeStyle = strokeStyle
            if (prevState !== null) {
                let prevPoint = shape.map.latLngToContainerPoint([prevState.lat, prevState.lon])
                ctx.moveTo(prevPoint.x, prevPoint.y)
            }

            ctx.lineTo(point.x, point.y)
            ctx.stroke()
            prevState = state
        }
    }

    static stateToColor(state: models.ObjectState): string {
        let velocity = state.velocity !== null ? state.velocity : 0
        // rescale knots [0 - 500] to hue [0 - 359].
        let color = (359 - 0) * ((velocity - 500) / (500 - 0)) + 0
        return "hsl(" + color + ",100%,50%)"
    }

    constructor(map: L.Map, states: models.ObjectState[]) {
        super({
            sceneFunc: TrailLine._sceneFunc,
            perfectDrawEnabled: false,
            listening: false,
        })

        this.map = map
        this.states = states
    }
}

class ObjectsOverlay extends EventTarget {
    private static INITIAL_OBJECTS_CAPACITY = 10000
    private static PLANE_BASE_SIZE = 7
    private static Z_AXIS_VECTOR3 = new THREE.Vector3(0, 0, 1)

    private mapOverlays: MapOverlays

    // Set in updateMapZoom()
    _mapZoom!: number
    protected canvasBoundingClientRect: DOMRect
    private objectBoundingBoxCenter: THREE.Vector3 = new THREE.Vector3()

    _mouseoverIcao: number | null

    scene: THREE.Scene
    renderer: THREE.WebGLRenderer
    camera: THREE.OrthographicCamera
    geometry: THREE.InstancedBufferGeometry
    mesh: THREE.Mesh

    offsetAttrData: Float32Array
    rotationAttrData: Float32Array
    colorAttrData: Float32Array
    idColorAttrData: Float32Array

    mapObjectToAttrsIndex: { [icao: number]: number }

    material: THREE.ShaderMaterial

    // https://threejsfundamentals.org/threejs/lessons/threejs-picking.html
    pickingScene: THREE.Scene
    pickingCamera: THREE.OrthographicCamera
    pickingTexture: THREE.WebGLRenderTarget
    pickingPixelBuffer: Uint8Array
    pickingMaterial: THREE.ShaderMaterial
    pickingMesh: THREE.Mesh

    latLngToContainerPoint: Function

    constructor(
        canvas: HTMLCanvasElement,
        latLngToContainerPoint: Function,
        mapZoom: number,
        mapOverlays: MapOverlays,
    ) {
        super()
        this.mapOverlays = mapOverlays
        this.canvasBoundingClientRect = canvas.getBoundingClientRect() as DOMRect
        this.latLngToContainerPoint = latLngToContainerPoint
        this._mouseoverIcao = null

        this.mapObjectToAttrsIndex = {}

        const capacity = ObjectsOverlay.INITIAL_OBJECTS_CAPACITY
        this.offsetAttrData = new Float32Array(capacity * 3)
        this.rotationAttrData = new Float32Array(capacity * 2)
        this.colorAttrData = new Float32Array(capacity * 3)
        this.idColorAttrData = new Float32Array(capacity * 3)

        this.scene = new THREE.Scene()
        this.camera = new THREE.OrthographicCamera(
            - canvas.clientWidth,
            canvas.clientWidth,
            canvas.clientHeight,
            -canvas.clientHeight,
            0,
            1,
        )

        this.camera.position.z = 1
        this.camera.lookAt(0, 0, 0)

        this.pickingCamera = this.camera.clone()
        this.renderer = new THREE.WebGLRenderer({
            antialias: true,
            alpha: true,
            canvas: canvas,
        })

        this.renderer.setPixelRatio(window.devicePixelRatio)

        this.geometry = new THREE.InstancedBufferGeometry()
        const planeGeometry = new THREE.PlaneGeometry(ObjectsOverlay.PLANE_BASE_SIZE, ObjectsOverlay.PLANE_BASE_SIZE)
        this.geometry.fromGeometry(planeGeometry)
        this.geometry.computeBoundingBox()

        let offsetAttr = new THREE.InstancedBufferAttribute(this.offsetAttrData, 2)
        offsetAttr.dynamic = true
        this.geometry.addAttribute('offset', offsetAttr)

        let rotationAttr = new THREE.InstancedBufferAttribute(this.rotationAttrData, 2)
        rotationAttr.dynamic = true
        this.geometry.addAttribute('rotation', rotationAttr)

        let colorAttr = new THREE.InstancedBufferAttribute(this.colorAttrData, 3)
        colorAttr.dynamic = false
        this.geometry.addAttribute('color', colorAttr)

        this.geometry.addAttribute(
            'idColor',
            new THREE.InstancedBufferAttribute(this.idColorAttrData, 3),
        )

        let vertexShader = `
            #ifdef PICKING
            attribute vec3 idColor;
            varying vec3 vidColor;
            #endif
            attribute vec2 offset;
            attribute vec2 rotation;
            uniform int scale;
            attribute vec3 color;
            varying vec3 vColor;
            varying vec2 vUv;

            void main() {
            #ifdef PICKING
                vidColor = idColor;
            #endif
                vUv = uv;
                vColor = color;
                vec3 rotatedPosition = vec3(
                    position.x * rotation.y + position.y * rotation.x,
                    position.y * rotation.y - position.x * rotation.x,
                    0);
                rotatedPosition *= vec3(scale, scale, 1.0);
                gl_Position = (projectionMatrix * modelViewMatrix * vec4(rotatedPosition + vec3(offset, 0.0), 1.0));
            }
        `

        var fragmentShader = `
            varying vec3 vColor;
            varying vec2 vUv;
            uniform sampler2D texture;
            void main() {
                vec4 tex = texture2D(texture, vUv);
                gl_FragColor = vec4(tex.r + vColor.r, tex.g + vColor.g, tex.b + vColor.b, tex.a);
            }
        `

        const texture = new THREE.TextureLoader().load("airplane.png");
        texture.minFilter = THREE.NearestFilter
        texture.magFilter = THREE.NearestFilter

        this.material = new THREE.ShaderMaterial({
            vertexShader: vertexShader,
            fragmentShader: fragmentShader,
            uniforms: {
                // Set in updateMapZoom()
                scale: { value: new THREE.Uniform(0) },
                texture: { value: texture },
            },
            transparent: true,
        })

        this.pickingPixelBuffer = new Uint8Array(4)
        this.pickingScene = new THREE.Scene()
        this.pickingScene.background = new THREE.Color(0);

        this.pickingTexture = new THREE.WebGLRenderTarget(
            canvas.clientWidth,
            canvas.clientHeight,
        )
        this.pickingTexture.texture.generateMipmaps = false
        this.pickingTexture.texture.minFilter = THREE.LinearFilter

        var pickingShader = `
            varying vec3 vidColor;
            void main(void) {
                gl_FragColor = vec4(vidColor,1.0);
            }
        `

        this.pickingMaterial = new THREE.ShaderMaterial({
            vertexShader: vertexShader,
            fragmentShader: pickingShader,
            transparent: false,
            uniforms: {
                // Set in updateMapZoom()
                scale: new THREE.Uniform(0),
            },
            defines: {
                PICKING: true,
            },
        })

        this.mesh = new THREE.Mesh(this.geometry, this.material)
        this.scene.add(this.mesh)

        this.pickingMesh = new THREE.Mesh(this.geometry, this.pickingMaterial)
        this.pickingScene.add(this.pickingMesh)

        this.updateMapZoom(mapZoom)

        // Setup Event Listeners.
        canvas.addEventListener('click', this.onMouseClick.bind(this))
        canvas.addEventListener(
            'mousemove',
            debounce(this._onMouseMove.bind(this), 10),
        )
    }

    _getIcaoAtPosition(clientX: number, clientY: number): number | null {
        this.renderer.setRenderTarget(this.pickingTexture)
        this.renderer.render(this.pickingScene, this.pickingCamera)
        this.renderer.readRenderTargetPixels(
            this.pickingTexture,
            clientX,
            this.pickingTexture.height - clientY + this.canvasBoundingClientRect.top,
            1,
            1,
            this.pickingPixelBuffer,
        )

        this.renderer.setRenderTarget(null)
        let icao = (
            (this.pickingPixelBuffer[0] << 16) |
            (this.pickingPixelBuffer[1] << 8) |
            (this.pickingPixelBuffer[2])
        )
        return icao || null
    }

    public setObjects(mapObjects: MapObjectsMap) {
        let offsetAttr = this.geometry.getAttribute('offset') as THREE.InstancedBufferAttribute
        let idColorAttr = this.geometry.getAttribute('idColor') as THREE.InstancedBufferAttribute
        let rotationAttr = this.geometry.getAttribute('rotation') as THREE.InstancedBufferAttribute
        let colorAttr = this.geometry.getAttribute('color') as THREE.InstancedBufferAttribute

        let idx = 0

        // reusable color instance to get the icao in rgb.
        let color = new THREE.Color()

        for (const icao in mapObjects) {
            const mapObject = mapObjects[icao]
            let state = mapObject.getEffectiveState()

            // TODO: Do the math ourselves.
            color.setHex(state.icao)
            idColorAttr.setXYZ(idx, color.r, color.g, color.b)

            // Rotation
            if (state.heading !== null) {
                let radHeading = state.heading * Math.PI / 180
                rotationAttr.setXY(idx, Math.sin(radHeading), Math.cos(radHeading))
            }

            // Offset
            // Recompute coordinates from CSS system to WebGL.
            let pos = this.latLngToContainerPoint([state.lat, state.lon])
            let x = pos.x * 2 - this.renderer.domElement.clientWidth
            let y = -2 * pos.y + this.renderer.domElement.clientHeight
            offsetAttr.setXY(idx, x, y)

            // Color - preserve.
            const prevIdx = this.mapObjectToAttrsIndex[icao]
            if (prevIdx !== undefined) {
                const r = colorAttr.getX(prevIdx)
                const g = colorAttr.getY(prevIdx)
                const b = colorAttr.getZ(prevIdx)
                colorAttr.setXYZ(idx, r, g, b)
            }

            this.mapObjectToAttrsIndex[icao] = idx

            idx += 1
        }

        idColorAttr.needsUpdate = true
        rotationAttr.needsUpdate = true
        offsetAttr.needsUpdate = true

        this.geometry.maxInstancedCount = idx
    }

    _updateColor(mapObject: MapObject, r: number, g: number, b: number) {
        // This is now only used as a color offset on top of the original
        // yellow image, which is stupid, but it's good enough for now.
        let idx = this.mapObjectToAttrsIndex[mapObject.state.icao]
        let colorAttr = this.geometry.attributes.color as THREE.InstancedBufferAttribute
        colorAttr.setXYZ(idx, r, g, b)
        colorAttr.needsUpdate = true
        window.requestAnimationFrame(() => this.render())
    }

    public updateMapZoom(mapZoom: number) {
        this._mapZoom = mapZoom
        this.material.uniforms.scale.value = mapZoom
        this.pickingMaterial.uniforms.scale.value = mapZoom
        this.geometry.boundingBox.getCenter(this.objectBoundingBoxCenter)
        this.objectBoundingBoxCenter.multiplyScalar(mapZoom)
    }

    public styleObjectNormal(mapObject: MapObject) {
        this._updateColor(mapObject, 0.0, 0.0, 0.0)
    }

    public styleObjectHighlighted(mapObject: MapObject) {
        this._updateColor(mapObject, 0.8, -0.3, -0.3)
    }

    public render() {
        this.renderer.render(this.scene, this.camera)
    }

    private _onMouseMove(ev: MouseEvent) {
        if (this.mapOverlays.dragging) {
            return false
        }

        let mouseIcao = this._getIcaoAtPosition(ev.clientX, ev.clientY)
        if (mouseIcao === this._mouseoverIcao) {
            return
        }

        if (mouseIcao === null) {
            if (this._mouseoverIcao === null) {
                return
            }
            else {
                this.dispatchEvent(new CustomEvent('mouseleave-object', { detail: { icao: this._mouseoverIcao } }))
                this._mouseoverIcao = null
            }
        }
        else {
            if (!(mouseIcao in this.mapObjectToAttrsIndex)) {
                // We're racing against setObjects updating the index.
                // It's possible that momentarily we're out of sync.
                return
            }

            // Handle old marker.
            if (this._mouseoverIcao !== null) {
                this.dispatchEvent(new CustomEvent('mouseleave-object', { detail: { icao: this._mouseoverIcao } }))
            }

            // Handle new marker.
            this.dispatchEvent(new CustomEvent('mouseenter-object', { detail: { icao: mouseIcao } }))
            this._mouseoverIcao = mouseIcao
        }
    }

    private onMouseClick(ev: MouseEvent) {
        if (this.mapOverlays.dragging) {
            return false
        }

        let mouseIcao = this._getIcaoAtPosition(ev.clientX, ev.clientY)
        if (mouseIcao === null) {
            this.dispatchEvent(new Event('object-deselect'))
        }
        else {
            this.dispatchEvent(new CustomEvent('object-select', { detail: { icao: mouseIcao } }))
        }
    }

    public getTipToCenterOffsetX(heading: number): number {
        // Apply angle, capture offset, revert angle.
        const headingRad = heading * THREE.Math.DEG2RAD
        this.objectBoundingBoxCenter.applyAxisAngle(ObjectsOverlay.Z_AXIS_VECTOR3, - headingRad)
        let x = this.objectBoundingBoxCenter.x
        this.objectBoundingBoxCenter.applyAxisAngle(ObjectsOverlay.Z_AXIS_VECTOR3, headingRad)
        return x
    }
}
