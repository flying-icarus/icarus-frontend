import { ACTION_TYPES } from '../actions'
import * as models from '../models'

export interface IState {
    playbackEnabled: boolean,
    playbackTimestamp: number | null,
    playbackSpeed: number | null,
    playbackLastRequestedTimestamp: number | null,
    bbox: models.BoundingBox,
    states: models.ObjectStatesMap,
    history: models.FlightHistory | null,
    selectedObject: number | null,
    mouseOverObject: number | null,
}

const BBOX_CRETE: models.BoundingBox = [22.851563, 34.379713, 27.070313, 37.857507]
function map(state: IState | undefined, action: any): IState {
    if (state === undefined) {
        let bbox: models.BoundingBox
        let bboxStorage = window.localStorage.getItem('view-bbox')
        if (bboxStorage !== null) {
            let bboxSplit = bboxStorage.split(",")
            if (bboxSplit.length === 4) {
                bbox = bboxStorage.split(",").map(parseFloat) as models.BoundingBox
            }
            else {
                bbox = BBOX_CRETE
            }
        } else {
            bbox = BBOX_CRETE
        }

        return {
            states: {},
            playbackLastRequestedTimestamp: null,
            history: null,
            selectedObject: null,
            mouseOverObject: null,
            playbackEnabled: false,
            playbackTimestamp: null,
            playbackSpeed: null,
            bbox,
        }
    }

    switch (action.type) {
        case ACTION_TYPES.SET_VIEW_BBOX:
            return {
                ...state,
                bbox: action.bbox,
            }
        case ACTION_TYPES.SET_OBJECT_STATES:
            // If the selected object is not in the new states, unselect it.
            let selectedObject = state.selectedObject
            let mouseOverObject = state.mouseOverObject
            let history = state.history
            if ((selectedObject !== null) &&
                !(selectedObject in action.states)) {
                selectedObject = null
                history = null
            }
            if ((mouseOverObject !== null) &&
                !(mouseOverObject in action.states)) {
                mouseOverObject = null
            }
            return {
                ...state,
                states: action.states,
                selectedObject,
                mouseOverObject,
                history,
            }
        case ACTION_TYPES.REQUEST_OBJECT_TRAIL:
            return {
                ...state,
                history: null,
            }
        case ACTION_TYPES.SET_OBJECT_TRAIL:
            return {
                ...state,
                history: action.history,
            }
        case ACTION_TYPES.MAP_OBJECT_SELECTED:
            if (!(action.icao in state.states)) {
                throw new Error("selected object not part of the known states")
            }

            return {
                ...state,
                selectedObject: action.icao
            }
        case ACTION_TYPES.MAP_OBJECT_DESELECTED:
            return {
                ...state,
                selectedObject: null,
                history: null,
            }
        case ACTION_TYPES.MAP_OBJECT_MOUSEENTER:
            return {
                ...state,
                mouseOverObject: action.icao,
            }
        case ACTION_TYPES.MAP_OBJECT_MOUSELEAVE:
            return {
                ...state,
                mouseOverObject: null,
            }
        case ACTION_TYPES.PLAYBACK_ENABLE:
            return {
                ...state,
                playbackEnabled: true,
                playbackTimestamp: action.timestamp,
                playbackSpeed: action.speed,
            }
        case ACTION_TYPES.PLAYBACK_DISABLE:
            return {
                ...state,
                playbackEnabled: false,
                playbackTimestamp: null,
                playbackLastRequestedTimestamp: null,
                playbackSpeed: null,
            }
        case ACTION_TYPES.REQUEST_STATES:
            if (state.playbackEnabled === true) {
                return {
                    ...state,
                    playbackLastRequestedTimestamp: action.timestamp,
                }
            }
            else {
                return state
            }
        default:
            return state
    }
}

export default map
