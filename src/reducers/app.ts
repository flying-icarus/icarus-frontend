import { ACTION_TYPES } from '../actions'

export interface IState {
    error: Error | null,
    showDebugInfo: boolean
}

const app = (state: IState | undefined, action: any): IState => {
    if (state === undefined) {
        return {
            error: null,
            showDebugInfo: false,
        }
    }

    switch (action.type) {
        case ACTION_TYPES.SET_ROOT_ERROR:
            return {
                error: action.error,
                ...state,
            }
        default:
            return state
    }
}

export default app
