import { combineReducers } from 'redux'
import map from './map'
import app from './app'
import history from './history'

export default combineReducers({
    app,
    map,
    history,
})
