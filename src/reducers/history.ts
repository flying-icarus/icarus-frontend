import { ACTION_TYPES } from '../actions'
import * as models from '../models'

export interface IState {
    loading: boolean,
    error: Error | null,
    flights: models.Flight[],
}

function history(state: IState | undefined, action: any): IState {
    if (state === undefined) {
        return {
            loading: false,
            error: null,
            flights: [],
        }
    }

    switch (action.type) {
        case ACTION_TYPES.REQUEST_OBJECT_HISTORY:
            return {
                ...state,
                loading: true,
            }
        case ACTION_TYPES.SET_OBJECT_HISTORY:
            return {
                ...state,
                loading: false,
                flights: action.history.flights,
            }
        default:
            return state
    }
}

export default history;
