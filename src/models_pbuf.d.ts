import * as $protobuf from "protobufjs";
/** Properties of an ObjectStateLossy. */
export interface IObjectStateLossy {

    /** ObjectStateLossy reportedAt */
    reportedAt?: (number|null);

    /** ObjectStateLossy icao */
    icao?: (number|null);

    /** ObjectStateLossy callsign */
    callsign?: (string|null);

    /** ObjectStateLossy lat */
    lat?: (number|null);

    /** ObjectStateLossy lon */
    lon?: (number|null);

    /** ObjectStateLossy velocity */
    velocity?: (number|null);

    /** ObjectStateLossy heading */
    heading?: (number|null);

    /** ObjectStateLossy baroAltitude */
    baroAltitude?: (number|null);

    /** ObjectStateLossy geoAltitude */
    geoAltitude?: (number|null);

    /** ObjectStateLossy verticalRate */
    verticalRate?: (number|null);

    /** ObjectStateLossy squawk */
    squawk?: (number|null);
}

/** Represents an ObjectStateLossy. */
export class ObjectStateLossy implements IObjectStateLossy {

    /**
     * Constructs a new ObjectStateLossy.
     * @param [properties] Properties to set
     */
    constructor(properties?: IObjectStateLossy);

    /** ObjectStateLossy reportedAt. */
    public reportedAt: number;

    /** ObjectStateLossy icao. */
    public icao: number;

    /** ObjectStateLossy callsign. */
    public callsign: string;

    /** ObjectStateLossy lat. */
    public lat: number;

    /** ObjectStateLossy lon. */
    public lon: number;

    /** ObjectStateLossy velocity. */
    public velocity: number;

    /** ObjectStateLossy heading. */
    public heading: number;

    /** ObjectStateLossy baroAltitude. */
    public baroAltitude: number;

    /** ObjectStateLossy geoAltitude. */
    public geoAltitude: number;

    /** ObjectStateLossy verticalRate. */
    public verticalRate: number;

    /** ObjectStateLossy squawk. */
    public squawk: number;

    /**
     * Creates a new ObjectStateLossy instance using the specified properties.
     * @param [properties] Properties to set
     * @returns ObjectStateLossy instance
     */
    public static create(properties?: IObjectStateLossy): ObjectStateLossy;

    /**
     * Encodes the specified ObjectStateLossy message. Does not implicitly {@link ObjectStateLossy.verify|verify} messages.
     * @param message ObjectStateLossy message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IObjectStateLossy, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified ObjectStateLossy message, length delimited. Does not implicitly {@link ObjectStateLossy.verify|verify} messages.
     * @param message ObjectStateLossy message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IObjectStateLossy, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes an ObjectStateLossy message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns ObjectStateLossy
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): ObjectStateLossy;

    /**
     * Decodes an ObjectStateLossy message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns ObjectStateLossy
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): ObjectStateLossy;

    /**
     * Verifies an ObjectStateLossy message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates an ObjectStateLossy message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns ObjectStateLossy
     */
    public static fromObject(object: { [k: string]: any }): ObjectStateLossy;

    /**
     * Creates a plain object from an ObjectStateLossy message. Also converts values to other types if specified.
     * @param message ObjectStateLossy
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: ObjectStateLossy, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this ObjectStateLossy to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of an ObjectStatesLossy. */
export interface IObjectStatesLossy {

    /** ObjectStatesLossy states */
    states?: (IObjectStateLossy[]|null);
}

/** Represents an ObjectStatesLossy. */
export class ObjectStatesLossy implements IObjectStatesLossy {

    /**
     * Constructs a new ObjectStatesLossy.
     * @param [properties] Properties to set
     */
    constructor(properties?: IObjectStatesLossy);

    /** ObjectStatesLossy states. */
    public states: IObjectStateLossy[];

    /**
     * Creates a new ObjectStatesLossy instance using the specified properties.
     * @param [properties] Properties to set
     * @returns ObjectStatesLossy instance
     */
    public static create(properties?: IObjectStatesLossy): ObjectStatesLossy;

    /**
     * Encodes the specified ObjectStatesLossy message. Does not implicitly {@link ObjectStatesLossy.verify|verify} messages.
     * @param message ObjectStatesLossy message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IObjectStatesLossy, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified ObjectStatesLossy message, length delimited. Does not implicitly {@link ObjectStatesLossy.verify|verify} messages.
     * @param message ObjectStatesLossy message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IObjectStatesLossy, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes an ObjectStatesLossy message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns ObjectStatesLossy
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): ObjectStatesLossy;

    /**
     * Decodes an ObjectStatesLossy message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns ObjectStatesLossy
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): ObjectStatesLossy;

    /**
     * Verifies an ObjectStatesLossy message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates an ObjectStatesLossy message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns ObjectStatesLossy
     */
    public static fromObject(object: { [k: string]: any }): ObjectStatesLossy;

    /**
     * Creates a plain object from an ObjectStatesLossy message. Also converts values to other types if specified.
     * @param message ObjectStatesLossy
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: ObjectStatesLossy, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this ObjectStatesLossy to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of an ObjectState. */
export interface IObjectState {

    /** ObjectState reportedAt */
    reportedAt?: (number|null);

    /** ObjectState icao */
    icao?: (number|null);

    /** ObjectState callsign */
    callsign?: (string|null);

    /** ObjectState lat */
    lat?: (number|null);

    /** ObjectState lon */
    lon?: (number|null);

    /** ObjectState velocity */
    velocity?: (number|null);

    /** ObjectState heading */
    heading?: (number|null);

    /** ObjectState baroAltitude */
    baroAltitude?: (number|null);

    /** ObjectState geoAltitude */
    geoAltitude?: (number|null);

    /** ObjectState verticalRate */
    verticalRate?: (number|null);

    /** ObjectState squawk */
    squawk?: (number|null);
}

/** Represents an ObjectState. */
export class ObjectState implements IObjectState {

    /**
     * Constructs a new ObjectState.
     * @param [properties] Properties to set
     */
    constructor(properties?: IObjectState);

    /** ObjectState reportedAt. */
    public reportedAt: number;

    /** ObjectState icao. */
    public icao: number;

    /** ObjectState callsign. */
    public callsign: string;

    /** ObjectState lat. */
    public lat: number;

    /** ObjectState lon. */
    public lon: number;

    /** ObjectState velocity. */
    public velocity: number;

    /** ObjectState heading. */
    public heading: number;

    /** ObjectState baroAltitude. */
    public baroAltitude: number;

    /** ObjectState geoAltitude. */
    public geoAltitude: number;

    /** ObjectState verticalRate. */
    public verticalRate: number;

    /** ObjectState squawk. */
    public squawk: number;

    /**
     * Creates a new ObjectState instance using the specified properties.
     * @param [properties] Properties to set
     * @returns ObjectState instance
     */
    public static create(properties?: IObjectState): ObjectState;

    /**
     * Encodes the specified ObjectState message. Does not implicitly {@link ObjectState.verify|verify} messages.
     * @param message ObjectState message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IObjectState, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified ObjectState message, length delimited. Does not implicitly {@link ObjectState.verify|verify} messages.
     * @param message ObjectState message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IObjectState, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes an ObjectState message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns ObjectState
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): ObjectState;

    /**
     * Decodes an ObjectState message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns ObjectState
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): ObjectState;

    /**
     * Verifies an ObjectState message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates an ObjectState message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns ObjectState
     */
    public static fromObject(object: { [k: string]: any }): ObjectState;

    /**
     * Creates a plain object from an ObjectState message. Also converts values to other types if specified.
     * @param message ObjectState
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: ObjectState, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this ObjectState to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of an ObjectStatesMap. */
export interface IObjectStatesMap {

    /** ObjectStatesMap states */
    states?: ({ [k: string]: IObjectState }|null);
}

/** Represents an ObjectStatesMap. */
export class ObjectStatesMap implements IObjectStatesMap {

    /**
     * Constructs a new ObjectStatesMap.
     * @param [properties] Properties to set
     */
    constructor(properties?: IObjectStatesMap);

    /** ObjectStatesMap states. */
    public states: { [k: string]: IObjectState };

    /**
     * Creates a new ObjectStatesMap instance using the specified properties.
     * @param [properties] Properties to set
     * @returns ObjectStatesMap instance
     */
    public static create(properties?: IObjectStatesMap): ObjectStatesMap;

    /**
     * Encodes the specified ObjectStatesMap message. Does not implicitly {@link ObjectStatesMap.verify|verify} messages.
     * @param message ObjectStatesMap message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IObjectStatesMap, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified ObjectStatesMap message, length delimited. Does not implicitly {@link ObjectStatesMap.verify|verify} messages.
     * @param message ObjectStatesMap message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IObjectStatesMap, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes an ObjectStatesMap message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns ObjectStatesMap
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): ObjectStatesMap;

    /**
     * Decodes an ObjectStatesMap message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns ObjectStatesMap
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): ObjectStatesMap;

    /**
     * Verifies an ObjectStatesMap message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates an ObjectStatesMap message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns ObjectStatesMap
     */
    public static fromObject(object: { [k: string]: any }): ObjectStatesMap;

    /**
     * Creates a plain object from an ObjectStatesMap message. Also converts values to other types if specified.
     * @param message ObjectStatesMap
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: ObjectStatesMap, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this ObjectStatesMap to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of an ObjectStates. */
export interface IObjectStates {

    /** ObjectStates states */
    states?: (IObjectState[]|null);
}

/** Represents an ObjectStates. */
export class ObjectStates implements IObjectStates {

    /**
     * Constructs a new ObjectStates.
     * @param [properties] Properties to set
     */
    constructor(properties?: IObjectStates);

    /** ObjectStates states. */
    public states: IObjectState[];

    /**
     * Creates a new ObjectStates instance using the specified properties.
     * @param [properties] Properties to set
     * @returns ObjectStates instance
     */
    public static create(properties?: IObjectStates): ObjectStates;

    /**
     * Encodes the specified ObjectStates message. Does not implicitly {@link ObjectStates.verify|verify} messages.
     * @param message ObjectStates message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IObjectStates, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified ObjectStates message, length delimited. Does not implicitly {@link ObjectStates.verify|verify} messages.
     * @param message ObjectStates message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IObjectStates, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes an ObjectStates message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns ObjectStates
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): ObjectStates;

    /**
     * Decodes an ObjectStates message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns ObjectStates
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): ObjectStates;

    /**
     * Verifies an ObjectStates message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates an ObjectStates message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns ObjectStates
     */
    public static fromObject(object: { [k: string]: any }): ObjectStates;

    /**
     * Creates a plain object from an ObjectStates message. Also converts values to other types if specified.
     * @param message ObjectStates
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: ObjectStates, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this ObjectStates to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of a Picture. */
export interface IPicture {

    /** Picture thumbnailUrl */
    thumbnailUrl?: (string|null);

    /** Picture picturePageUrl */
    picturePageUrl?: (string|null);

    /** Picture photographerFullname */
    photographerFullname?: (string|null);
}

/** Represents a Picture. */
export class Picture implements IPicture {

    /**
     * Constructs a new Picture.
     * @param [properties] Properties to set
     */
    constructor(properties?: IPicture);

    /** Picture thumbnailUrl. */
    public thumbnailUrl: string;

    /** Picture picturePageUrl. */
    public picturePageUrl: string;

    /** Picture photographerFullname. */
    public photographerFullname: string;

    /**
     * Creates a new Picture instance using the specified properties.
     * @param [properties] Properties to set
     * @returns Picture instance
     */
    public static create(properties?: IPicture): Picture;

    /**
     * Encodes the specified Picture message. Does not implicitly {@link Picture.verify|verify} messages.
     * @param message Picture message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IPicture, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified Picture message, length delimited. Does not implicitly {@link Picture.verify|verify} messages.
     * @param message Picture message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IPicture, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a Picture message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns Picture
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Picture;

    /**
     * Decodes a Picture message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns Picture
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Picture;

    /**
     * Verifies a Picture message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a Picture message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns Picture
     */
    public static fromObject(object: { [k: string]: any }): Picture;

    /**
     * Creates a plain object from a Picture message. Also converts values to other types if specified.
     * @param message Picture
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: Picture, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this Picture to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of a FlightHistory. */
export interface IFlightHistory {

    /** FlightHistory states */
    states?: (IObjectState[]|null);

    /** FlightHistory pictures */
    pictures?: (IPicture[]|null);
}

/** Represents a FlightHistory. */
export class FlightHistory implements IFlightHistory {

    /**
     * Constructs a new FlightHistory.
     * @param [properties] Properties to set
     */
    constructor(properties?: IFlightHistory);

    /** FlightHistory states. */
    public states: IObjectState[];

    /** FlightHistory pictures. */
    public pictures: IPicture[];

    /**
     * Creates a new FlightHistory instance using the specified properties.
     * @param [properties] Properties to set
     * @returns FlightHistory instance
     */
    public static create(properties?: IFlightHistory): FlightHistory;

    /**
     * Encodes the specified FlightHistory message. Does not implicitly {@link FlightHistory.verify|verify} messages.
     * @param message FlightHistory message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IFlightHistory, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified FlightHistory message, length delimited. Does not implicitly {@link FlightHistory.verify|verify} messages.
     * @param message FlightHistory message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IFlightHistory, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a FlightHistory message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns FlightHistory
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): FlightHistory;

    /**
     * Decodes a FlightHistory message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns FlightHistory
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): FlightHistory;

    /**
     * Verifies a FlightHistory message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a FlightHistory message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns FlightHistory
     */
    public static fromObject(object: { [k: string]: any }): FlightHistory;

    /**
     * Creates a plain object from a FlightHistory message. Also converts values to other types if specified.
     * @param message FlightHistory
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: FlightHistory, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this FlightHistory to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}
