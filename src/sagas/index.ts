import { select, call, put, takeLatest, all, delay, race, take } from 'redux-saga/effects'
import * as models from '../models'
import * as actions from '../actions'
import * as api from '../api'
import config from '../config'

function* requestStates(action: actions.IRequestStates) {
    try {
        let states = yield call(api.getObjectStates, action.timestamp, action.bbox)
        yield put(actions.setStates(states))
    }
    catch (err) {
        let setRootError: actions.ISetRootError = {
            type: actions.ACTION_TYPES.SET_ROOT_ERROR,
            error: err,
        }

        return yield put(setRootError)
    }
}

function* watchRequestStates() {
    yield takeLatest(actions.ACTION_TYPES.REQUEST_STATES, requestStates)
}

function* watchRequestObjectHistory() {
    yield takeLatest(
        actions.ACTION_TYPES.REQUEST_OBJECT_HISTORY,
        function* requestObjectHistory(action: actions.IRequestObjectHistoryOut) {
            let history = yield call(api.getObjectHistory, { ...action.filter })
            yield put(actions.setObjectHistory(history))
        }
    )
}

function* putRequestStates(timestamp: number, bbox: models.BoundingBox) {
    let action: actions.IRequestStates = {
        type: actions.ACTION_TYPES.REQUEST_STATES,
        timestamp,
        bbox,
    }

    yield put(action)
}

function* scheduleRequestStates() {
    const VISIBILITY_STATE_VISIBLE = String("visible")

    while (true) {
        const effects = yield race({
            mapBBoxChanged: take(actions.ACTION_TYPES.SET_VIEW_BBOX),
            playbackEnabled: take(actions.ACTION_TYPES.PLAYBACK_ENABLE),
            playbackDisabled: take(actions.ACTION_TYPES.PLAYBACK_DISABLE),
            interval: delay(config.REFRESH_STATES_EVERY),
        })

        if (document.visibilityState !== VISIBILITY_STATE_VISIBLE) {
            continue
        }

        let bbox = yield select((state: models.IRootState) => state.map.bbox)
        let playback = yield select((state: models.IRootState) => ({
            enabled: state.map.playbackEnabled,
            timestamp: state.map.playbackTimestamp,
            speed: state.map.playbackSpeed,
            lastRequestedTimestamp: state.map.playbackLastRequestedTimestamp,
        }))

        let timestamp, speed
        if (playback.enabled) {
            speed = playback.speed
            if (effects.playbackEnabled) {
                timestamp = playback.timestamp
            }
            else {
                timestamp = playback.lastRequestedTimestamp! + (config.REFRESH_STATES_EVERY * speed)
            }
        }
        else {
            timestamp = Date.now()
            speed = 1.0
        }

        yield putRequestStates(timestamp, bbox)
    }
}

function* requestObjectTrail(action: actions.IRequestObjectTrail) {
    try {
        let states = yield call(api.getObjectTrail, action.icao)
        yield put(actions.setObjectTrail(states))
    }
    catch (err) {
        let setRootError: actions.ISetRootError = {
            type: actions.ACTION_TYPES.SET_ROOT_ERROR,
            error: err,
        }

        return yield put(setRootError)
    }
}

function* watchRequestObjectTrail() {
    yield takeLatest(actions.ACTION_TYPES.REQUEST_OBJECT_TRAIL, requestObjectTrail)
}

function* onObjectSelect(action: actions.IMapObjectSelected) {
    let requestObjectTrail: actions.IRequestObjectTrail = {
        type: actions.ACTION_TYPES.REQUEST_OBJECT_TRAIL,
        icao: action.icao
    }

    yield put(requestObjectTrail)
}

function* watchObjectSelected() {
    yield takeLatest(actions.ACTION_TYPES.MAP_OBJECT_SELECTED, onObjectSelect)
}


export default function* rootSaga() {
    yield all([
        scheduleRequestStates(),
        watchRequestStates(),
        watchObjectSelected(),
        watchRequestObjectTrail(),
        watchRequestObjectHistory(),
    ])
}
