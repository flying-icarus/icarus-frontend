/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
import * as $protobuf from "protobufjs/minimal";

// Common aliases
const $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
const $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

export const ObjectStateLossy = $root.ObjectStateLossy = (() => {

    /**
     * Properties of an ObjectStateLossy.
     * @exports IObjectStateLossy
     * @interface IObjectStateLossy
     * @property {number|null} [reportedAt] ObjectStateLossy reportedAt
     * @property {number|null} [icao] ObjectStateLossy icao
     * @property {string|null} [callsign] ObjectStateLossy callsign
     * @property {number|null} [lat] ObjectStateLossy lat
     * @property {number|null} [lon] ObjectStateLossy lon
     * @property {number|null} [velocity] ObjectStateLossy velocity
     * @property {number|null} [heading] ObjectStateLossy heading
     * @property {number|null} [baroAltitude] ObjectStateLossy baroAltitude
     * @property {number|null} [geoAltitude] ObjectStateLossy geoAltitude
     * @property {number|null} [verticalRate] ObjectStateLossy verticalRate
     * @property {number|null} [squawk] ObjectStateLossy squawk
     */

    /**
     * Constructs a new ObjectStateLossy.
     * @exports ObjectStateLossy
     * @classdesc Represents an ObjectStateLossy.
     * @implements IObjectStateLossy
     * @constructor
     * @param {IObjectStateLossy=} [properties] Properties to set
     */
    function ObjectStateLossy(properties) {
        if (properties)
            for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * ObjectStateLossy reportedAt.
     * @member {number} reportedAt
     * @memberof ObjectStateLossy
     * @instance
     */
    ObjectStateLossy.prototype.reportedAt = 0;

    /**
     * ObjectStateLossy icao.
     * @member {number} icao
     * @memberof ObjectStateLossy
     * @instance
     */
    ObjectStateLossy.prototype.icao = 0;

    /**
     * ObjectStateLossy callsign.
     * @member {string} callsign
     * @memberof ObjectStateLossy
     * @instance
     */
    ObjectStateLossy.prototype.callsign = "";

    /**
     * ObjectStateLossy lat.
     * @member {number} lat
     * @memberof ObjectStateLossy
     * @instance
     */
    ObjectStateLossy.prototype.lat = 0;

    /**
     * ObjectStateLossy lon.
     * @member {number} lon
     * @memberof ObjectStateLossy
     * @instance
     */
    ObjectStateLossy.prototype.lon = 0;

    /**
     * ObjectStateLossy velocity.
     * @member {number} velocity
     * @memberof ObjectStateLossy
     * @instance
     */
    ObjectStateLossy.prototype.velocity = 0;

    /**
     * ObjectStateLossy heading.
     * @member {number} heading
     * @memberof ObjectStateLossy
     * @instance
     */
    ObjectStateLossy.prototype.heading = 0;

    /**
     * ObjectStateLossy baroAltitude.
     * @member {number} baroAltitude
     * @memberof ObjectStateLossy
     * @instance
     */
    ObjectStateLossy.prototype.baroAltitude = 0;

    /**
     * ObjectStateLossy geoAltitude.
     * @member {number} geoAltitude
     * @memberof ObjectStateLossy
     * @instance
     */
    ObjectStateLossy.prototype.geoAltitude = 0;

    /**
     * ObjectStateLossy verticalRate.
     * @member {number} verticalRate
     * @memberof ObjectStateLossy
     * @instance
     */
    ObjectStateLossy.prototype.verticalRate = 0;

    /**
     * ObjectStateLossy squawk.
     * @member {number} squawk
     * @memberof ObjectStateLossy
     * @instance
     */
    ObjectStateLossy.prototype.squawk = 0;

    /**
     * Creates a new ObjectStateLossy instance using the specified properties.
     * @function create
     * @memberof ObjectStateLossy
     * @static
     * @param {IObjectStateLossy=} [properties] Properties to set
     * @returns {ObjectStateLossy} ObjectStateLossy instance
     */
    ObjectStateLossy.create = function create(properties) {
        return new ObjectStateLossy(properties);
    };

    /**
     * Encodes the specified ObjectStateLossy message. Does not implicitly {@link ObjectStateLossy.verify|verify} messages.
     * @function encode
     * @memberof ObjectStateLossy
     * @static
     * @param {IObjectStateLossy} message ObjectStateLossy message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ObjectStateLossy.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.reportedAt != null && message.hasOwnProperty("reportedAt"))
            writer.uint32(/* id 1, wireType 5 =*/13).fixed32(message.reportedAt);
        if (message.icao != null && message.hasOwnProperty("icao"))
            writer.uint32(/* id 2, wireType 0 =*/16).uint32(message.icao);
        if (message.callsign != null && message.hasOwnProperty("callsign"))
            writer.uint32(/* id 3, wireType 2 =*/26).string(message.callsign);
        if (message.lat != null && message.hasOwnProperty("lat"))
            writer.uint32(/* id 4, wireType 0 =*/32).sint32(message.lat);
        if (message.lon != null && message.hasOwnProperty("lon"))
            writer.uint32(/* id 5, wireType 0 =*/40).sint32(message.lon);
        if (message.velocity != null && message.hasOwnProperty("velocity"))
            writer.uint32(/* id 6, wireType 0 =*/48).uint32(message.velocity);
        if (message.heading != null && message.hasOwnProperty("heading"))
            writer.uint32(/* id 7, wireType 0 =*/56).uint32(message.heading);
        if (message.baroAltitude != null && message.hasOwnProperty("baroAltitude"))
            writer.uint32(/* id 8, wireType 0 =*/64).uint32(message.baroAltitude);
        if (message.geoAltitude != null && message.hasOwnProperty("geoAltitude"))
            writer.uint32(/* id 9, wireType 0 =*/72).uint32(message.geoAltitude);
        if (message.verticalRate != null && message.hasOwnProperty("verticalRate"))
            writer.uint32(/* id 10, wireType 0 =*/80).int32(message.verticalRate);
        if (message.squawk != null && message.hasOwnProperty("squawk"))
            writer.uint32(/* id 11, wireType 0 =*/88).uint32(message.squawk);
        return writer;
    };

    /**
     * Encodes the specified ObjectStateLossy message, length delimited. Does not implicitly {@link ObjectStateLossy.verify|verify} messages.
     * @function encodeDelimited
     * @memberof ObjectStateLossy
     * @static
     * @param {IObjectStateLossy} message ObjectStateLossy message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ObjectStateLossy.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes an ObjectStateLossy message from the specified reader or buffer.
     * @function decode
     * @memberof ObjectStateLossy
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {ObjectStateLossy} ObjectStateLossy
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ObjectStateLossy.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        let end = length === undefined ? reader.len : reader.pos + length, message = new $root.ObjectStateLossy();
        while (reader.pos < end) {
            let tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.reportedAt = reader.fixed32();
                break;
            case 2:
                message.icao = reader.uint32();
                break;
            case 3:
                message.callsign = reader.string();
                break;
            case 4:
                message.lat = reader.sint32();
                break;
            case 5:
                message.lon = reader.sint32();
                break;
            case 6:
                message.velocity = reader.uint32();
                break;
            case 7:
                message.heading = reader.uint32();
                break;
            case 8:
                message.baroAltitude = reader.uint32();
                break;
            case 9:
                message.geoAltitude = reader.uint32();
                break;
            case 10:
                message.verticalRate = reader.int32();
                break;
            case 11:
                message.squawk = reader.uint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes an ObjectStateLossy message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof ObjectStateLossy
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {ObjectStateLossy} ObjectStateLossy
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ObjectStateLossy.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies an ObjectStateLossy message.
     * @function verify
     * @memberof ObjectStateLossy
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    ObjectStateLossy.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.reportedAt != null && message.hasOwnProperty("reportedAt"))
            if (!$util.isInteger(message.reportedAt))
                return "reportedAt: integer expected";
        if (message.icao != null && message.hasOwnProperty("icao"))
            if (!$util.isInteger(message.icao))
                return "icao: integer expected";
        if (message.callsign != null && message.hasOwnProperty("callsign"))
            if (!$util.isString(message.callsign))
                return "callsign: string expected";
        if (message.lat != null && message.hasOwnProperty("lat"))
            if (!$util.isInteger(message.lat))
                return "lat: integer expected";
        if (message.lon != null && message.hasOwnProperty("lon"))
            if (!$util.isInteger(message.lon))
                return "lon: integer expected";
        if (message.velocity != null && message.hasOwnProperty("velocity"))
            if (!$util.isInteger(message.velocity))
                return "velocity: integer expected";
        if (message.heading != null && message.hasOwnProperty("heading"))
            if (!$util.isInteger(message.heading))
                return "heading: integer expected";
        if (message.baroAltitude != null && message.hasOwnProperty("baroAltitude"))
            if (!$util.isInteger(message.baroAltitude))
                return "baroAltitude: integer expected";
        if (message.geoAltitude != null && message.hasOwnProperty("geoAltitude"))
            if (!$util.isInteger(message.geoAltitude))
                return "geoAltitude: integer expected";
        if (message.verticalRate != null && message.hasOwnProperty("verticalRate"))
            if (!$util.isInteger(message.verticalRate))
                return "verticalRate: integer expected";
        if (message.squawk != null && message.hasOwnProperty("squawk"))
            if (!$util.isInteger(message.squawk))
                return "squawk: integer expected";
        return null;
    };

    /**
     * Creates an ObjectStateLossy message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof ObjectStateLossy
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {ObjectStateLossy} ObjectStateLossy
     */
    ObjectStateLossy.fromObject = function fromObject(object) {
        if (object instanceof $root.ObjectStateLossy)
            return object;
        let message = new $root.ObjectStateLossy();
        if (object.reportedAt != null)
            message.reportedAt = object.reportedAt >>> 0;
        if (object.icao != null)
            message.icao = object.icao >>> 0;
        if (object.callsign != null)
            message.callsign = String(object.callsign);
        if (object.lat != null)
            message.lat = object.lat | 0;
        if (object.lon != null)
            message.lon = object.lon | 0;
        if (object.velocity != null)
            message.velocity = object.velocity >>> 0;
        if (object.heading != null)
            message.heading = object.heading >>> 0;
        if (object.baroAltitude != null)
            message.baroAltitude = object.baroAltitude >>> 0;
        if (object.geoAltitude != null)
            message.geoAltitude = object.geoAltitude >>> 0;
        if (object.verticalRate != null)
            message.verticalRate = object.verticalRate | 0;
        if (object.squawk != null)
            message.squawk = object.squawk >>> 0;
        return message;
    };

    /**
     * Creates a plain object from an ObjectStateLossy message. Also converts values to other types if specified.
     * @function toObject
     * @memberof ObjectStateLossy
     * @static
     * @param {ObjectStateLossy} message ObjectStateLossy
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    ObjectStateLossy.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        let object = {};
        if (options.defaults) {
            object.reportedAt = 0;
            object.icao = 0;
            object.callsign = "";
            object.lat = 0;
            object.lon = 0;
            object.velocity = 0;
            object.heading = 0;
            object.baroAltitude = 0;
            object.geoAltitude = 0;
            object.verticalRate = 0;
            object.squawk = 0;
        }
        if (message.reportedAt != null && message.hasOwnProperty("reportedAt"))
            object.reportedAt = message.reportedAt;
        if (message.icao != null && message.hasOwnProperty("icao"))
            object.icao = message.icao;
        if (message.callsign != null && message.hasOwnProperty("callsign"))
            object.callsign = message.callsign;
        if (message.lat != null && message.hasOwnProperty("lat"))
            object.lat = message.lat;
        if (message.lon != null && message.hasOwnProperty("lon"))
            object.lon = message.lon;
        if (message.velocity != null && message.hasOwnProperty("velocity"))
            object.velocity = message.velocity;
        if (message.heading != null && message.hasOwnProperty("heading"))
            object.heading = message.heading;
        if (message.baroAltitude != null && message.hasOwnProperty("baroAltitude"))
            object.baroAltitude = message.baroAltitude;
        if (message.geoAltitude != null && message.hasOwnProperty("geoAltitude"))
            object.geoAltitude = message.geoAltitude;
        if (message.verticalRate != null && message.hasOwnProperty("verticalRate"))
            object.verticalRate = message.verticalRate;
        if (message.squawk != null && message.hasOwnProperty("squawk"))
            object.squawk = message.squawk;
        return object;
    };

    /**
     * Converts this ObjectStateLossy to JSON.
     * @function toJSON
     * @memberof ObjectStateLossy
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    ObjectStateLossy.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return ObjectStateLossy;
})();

export const ObjectStatesLossy = $root.ObjectStatesLossy = (() => {

    /**
     * Properties of an ObjectStatesLossy.
     * @exports IObjectStatesLossy
     * @interface IObjectStatesLossy
     * @property {Array.<IObjectStateLossy>|null} [states] ObjectStatesLossy states
     */

    /**
     * Constructs a new ObjectStatesLossy.
     * @exports ObjectStatesLossy
     * @classdesc Represents an ObjectStatesLossy.
     * @implements IObjectStatesLossy
     * @constructor
     * @param {IObjectStatesLossy=} [properties] Properties to set
     */
    function ObjectStatesLossy(properties) {
        this.states = [];
        if (properties)
            for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * ObjectStatesLossy states.
     * @member {Array.<IObjectStateLossy>} states
     * @memberof ObjectStatesLossy
     * @instance
     */
    ObjectStatesLossy.prototype.states = $util.emptyArray;

    /**
     * Creates a new ObjectStatesLossy instance using the specified properties.
     * @function create
     * @memberof ObjectStatesLossy
     * @static
     * @param {IObjectStatesLossy=} [properties] Properties to set
     * @returns {ObjectStatesLossy} ObjectStatesLossy instance
     */
    ObjectStatesLossy.create = function create(properties) {
        return new ObjectStatesLossy(properties);
    };

    /**
     * Encodes the specified ObjectStatesLossy message. Does not implicitly {@link ObjectStatesLossy.verify|verify} messages.
     * @function encode
     * @memberof ObjectStatesLossy
     * @static
     * @param {IObjectStatesLossy} message ObjectStatesLossy message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ObjectStatesLossy.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.states != null && message.states.length)
            for (let i = 0; i < message.states.length; ++i)
                $root.ObjectStateLossy.encode(message.states[i], writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
        return writer;
    };

    /**
     * Encodes the specified ObjectStatesLossy message, length delimited. Does not implicitly {@link ObjectStatesLossy.verify|verify} messages.
     * @function encodeDelimited
     * @memberof ObjectStatesLossy
     * @static
     * @param {IObjectStatesLossy} message ObjectStatesLossy message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ObjectStatesLossy.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes an ObjectStatesLossy message from the specified reader or buffer.
     * @function decode
     * @memberof ObjectStatesLossy
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {ObjectStatesLossy} ObjectStatesLossy
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ObjectStatesLossy.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        let end = length === undefined ? reader.len : reader.pos + length, message = new $root.ObjectStatesLossy();
        while (reader.pos < end) {
            let tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.states && message.states.length))
                    message.states = [];
                message.states.push($root.ObjectStateLossy.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes an ObjectStatesLossy message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof ObjectStatesLossy
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {ObjectStatesLossy} ObjectStatesLossy
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ObjectStatesLossy.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies an ObjectStatesLossy message.
     * @function verify
     * @memberof ObjectStatesLossy
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    ObjectStatesLossy.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.states != null && message.hasOwnProperty("states")) {
            if (!Array.isArray(message.states))
                return "states: array expected";
            for (let i = 0; i < message.states.length; ++i) {
                let error = $root.ObjectStateLossy.verify(message.states[i]);
                if (error)
                    return "states." + error;
            }
        }
        return null;
    };

    /**
     * Creates an ObjectStatesLossy message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof ObjectStatesLossy
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {ObjectStatesLossy} ObjectStatesLossy
     */
    ObjectStatesLossy.fromObject = function fromObject(object) {
        if (object instanceof $root.ObjectStatesLossy)
            return object;
        let message = new $root.ObjectStatesLossy();
        if (object.states) {
            if (!Array.isArray(object.states))
                throw TypeError(".ObjectStatesLossy.states: array expected");
            message.states = [];
            for (let i = 0; i < object.states.length; ++i) {
                if (typeof object.states[i] !== "object")
                    throw TypeError(".ObjectStatesLossy.states: object expected");
                message.states[i] = $root.ObjectStateLossy.fromObject(object.states[i]);
            }
        }
        return message;
    };

    /**
     * Creates a plain object from an ObjectStatesLossy message. Also converts values to other types if specified.
     * @function toObject
     * @memberof ObjectStatesLossy
     * @static
     * @param {ObjectStatesLossy} message ObjectStatesLossy
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    ObjectStatesLossy.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        let object = {};
        if (options.arrays || options.defaults)
            object.states = [];
        if (message.states && message.states.length) {
            object.states = [];
            for (let j = 0; j < message.states.length; ++j)
                object.states[j] = $root.ObjectStateLossy.toObject(message.states[j], options);
        }
        return object;
    };

    /**
     * Converts this ObjectStatesLossy to JSON.
     * @function toJSON
     * @memberof ObjectStatesLossy
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    ObjectStatesLossy.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return ObjectStatesLossy;
})();

export const ObjectState = $root.ObjectState = (() => {

    /**
     * Properties of an ObjectState.
     * @exports IObjectState
     * @interface IObjectState
     * @property {number|null} [reportedAt] ObjectState reportedAt
     * @property {number|null} [icao] ObjectState icao
     * @property {string|null} [callsign] ObjectState callsign
     * @property {number|null} [lat] ObjectState lat
     * @property {number|null} [lon] ObjectState lon
     * @property {number|null} [velocity] ObjectState velocity
     * @property {number|null} [heading] ObjectState heading
     * @property {number|null} [baroAltitude] ObjectState baroAltitude
     * @property {number|null} [geoAltitude] ObjectState geoAltitude
     * @property {number|null} [verticalRate] ObjectState verticalRate
     * @property {number|null} [squawk] ObjectState squawk
     */

    /**
     * Constructs a new ObjectState.
     * @exports ObjectState
     * @classdesc Represents an ObjectState.
     * @implements IObjectState
     * @constructor
     * @param {IObjectState=} [properties] Properties to set
     */
    function ObjectState(properties) {
        if (properties)
            for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * ObjectState reportedAt.
     * @member {number} reportedAt
     * @memberof ObjectState
     * @instance
     */
    ObjectState.prototype.reportedAt = 0;

    /**
     * ObjectState icao.
     * @member {number} icao
     * @memberof ObjectState
     * @instance
     */
    ObjectState.prototype.icao = 0;

    /**
     * ObjectState callsign.
     * @member {string} callsign
     * @memberof ObjectState
     * @instance
     */
    ObjectState.prototype.callsign = "";

    /**
     * ObjectState lat.
     * @member {number} lat
     * @memberof ObjectState
     * @instance
     */
    ObjectState.prototype.lat = 0;

    /**
     * ObjectState lon.
     * @member {number} lon
     * @memberof ObjectState
     * @instance
     */
    ObjectState.prototype.lon = 0;

    /**
     * ObjectState velocity.
     * @member {number} velocity
     * @memberof ObjectState
     * @instance
     */
    ObjectState.prototype.velocity = 0;

    /**
     * ObjectState heading.
     * @member {number} heading
     * @memberof ObjectState
     * @instance
     */
    ObjectState.prototype.heading = 0;

    /**
     * ObjectState baroAltitude.
     * @member {number} baroAltitude
     * @memberof ObjectState
     * @instance
     */
    ObjectState.prototype.baroAltitude = 0;

    /**
     * ObjectState geoAltitude.
     * @member {number} geoAltitude
     * @memberof ObjectState
     * @instance
     */
    ObjectState.prototype.geoAltitude = 0;

    /**
     * ObjectState verticalRate.
     * @member {number} verticalRate
     * @memberof ObjectState
     * @instance
     */
    ObjectState.prototype.verticalRate = 0;

    /**
     * ObjectState squawk.
     * @member {number} squawk
     * @memberof ObjectState
     * @instance
     */
    ObjectState.prototype.squawk = 0;

    /**
     * Creates a new ObjectState instance using the specified properties.
     * @function create
     * @memberof ObjectState
     * @static
     * @param {IObjectState=} [properties] Properties to set
     * @returns {ObjectState} ObjectState instance
     */
    ObjectState.create = function create(properties) {
        return new ObjectState(properties);
    };

    /**
     * Encodes the specified ObjectState message. Does not implicitly {@link ObjectState.verify|verify} messages.
     * @function encode
     * @memberof ObjectState
     * @static
     * @param {IObjectState} message ObjectState message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ObjectState.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.reportedAt != null && message.hasOwnProperty("reportedAt"))
            writer.uint32(/* id 1, wireType 5 =*/13).fixed32(message.reportedAt);
        if (message.icao != null && message.hasOwnProperty("icao"))
            writer.uint32(/* id 2, wireType 0 =*/16).uint32(message.icao);
        if (message.callsign != null && message.hasOwnProperty("callsign"))
            writer.uint32(/* id 3, wireType 2 =*/26).string(message.callsign);
        if (message.lat != null && message.hasOwnProperty("lat"))
            writer.uint32(/* id 4, wireType 5 =*/37).float(message.lat);
        if (message.lon != null && message.hasOwnProperty("lon"))
            writer.uint32(/* id 5, wireType 5 =*/45).float(message.lon);
        if (message.velocity != null && message.hasOwnProperty("velocity"))
            writer.uint32(/* id 6, wireType 0 =*/48).uint32(message.velocity);
        if (message.heading != null && message.hasOwnProperty("heading"))
            writer.uint32(/* id 7, wireType 5 =*/61).float(message.heading);
        if (message.baroAltitude != null && message.hasOwnProperty("baroAltitude"))
            writer.uint32(/* id 8, wireType 0 =*/64).int32(message.baroAltitude);
        if (message.geoAltitude != null && message.hasOwnProperty("geoAltitude"))
            writer.uint32(/* id 9, wireType 0 =*/72).int32(message.geoAltitude);
        if (message.verticalRate != null && message.hasOwnProperty("verticalRate"))
            writer.uint32(/* id 10, wireType 0 =*/80).int32(message.verticalRate);
        if (message.squawk != null && message.hasOwnProperty("squawk"))
            writer.uint32(/* id 11, wireType 0 =*/88).uint32(message.squawk);
        return writer;
    };

    /**
     * Encodes the specified ObjectState message, length delimited. Does not implicitly {@link ObjectState.verify|verify} messages.
     * @function encodeDelimited
     * @memberof ObjectState
     * @static
     * @param {IObjectState} message ObjectState message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ObjectState.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes an ObjectState message from the specified reader or buffer.
     * @function decode
     * @memberof ObjectState
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {ObjectState} ObjectState
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ObjectState.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        let end = length === undefined ? reader.len : reader.pos + length, message = new $root.ObjectState();
        while (reader.pos < end) {
            let tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.reportedAt = reader.fixed32();
                break;
            case 2:
                message.icao = reader.uint32();
                break;
            case 3:
                message.callsign = reader.string();
                break;
            case 4:
                message.lat = reader.float();
                break;
            case 5:
                message.lon = reader.float();
                break;
            case 6:
                message.velocity = reader.uint32();
                break;
            case 7:
                message.heading = reader.float();
                break;
            case 8:
                message.baroAltitude = reader.int32();
                break;
            case 9:
                message.geoAltitude = reader.int32();
                break;
            case 10:
                message.verticalRate = reader.int32();
                break;
            case 11:
                message.squawk = reader.uint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes an ObjectState message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof ObjectState
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {ObjectState} ObjectState
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ObjectState.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies an ObjectState message.
     * @function verify
     * @memberof ObjectState
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    ObjectState.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.reportedAt != null && message.hasOwnProperty("reportedAt"))
            if (!$util.isInteger(message.reportedAt))
                return "reportedAt: integer expected";
        if (message.icao != null && message.hasOwnProperty("icao"))
            if (!$util.isInteger(message.icao))
                return "icao: integer expected";
        if (message.callsign != null && message.hasOwnProperty("callsign"))
            if (!$util.isString(message.callsign))
                return "callsign: string expected";
        if (message.lat != null && message.hasOwnProperty("lat"))
            if (typeof message.lat !== "number")
                return "lat: number expected";
        if (message.lon != null && message.hasOwnProperty("lon"))
            if (typeof message.lon !== "number")
                return "lon: number expected";
        if (message.velocity != null && message.hasOwnProperty("velocity"))
            if (!$util.isInteger(message.velocity))
                return "velocity: integer expected";
        if (message.heading != null && message.hasOwnProperty("heading"))
            if (typeof message.heading !== "number")
                return "heading: number expected";
        if (message.baroAltitude != null && message.hasOwnProperty("baroAltitude"))
            if (!$util.isInteger(message.baroAltitude))
                return "baroAltitude: integer expected";
        if (message.geoAltitude != null && message.hasOwnProperty("geoAltitude"))
            if (!$util.isInteger(message.geoAltitude))
                return "geoAltitude: integer expected";
        if (message.verticalRate != null && message.hasOwnProperty("verticalRate"))
            if (!$util.isInteger(message.verticalRate))
                return "verticalRate: integer expected";
        if (message.squawk != null && message.hasOwnProperty("squawk"))
            if (!$util.isInteger(message.squawk))
                return "squawk: integer expected";
        return null;
    };

    /**
     * Creates an ObjectState message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof ObjectState
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {ObjectState} ObjectState
     */
    ObjectState.fromObject = function fromObject(object) {
        if (object instanceof $root.ObjectState)
            return object;
        let message = new $root.ObjectState();
        if (object.reportedAt != null)
            message.reportedAt = object.reportedAt >>> 0;
        if (object.icao != null)
            message.icao = object.icao >>> 0;
        if (object.callsign != null)
            message.callsign = String(object.callsign);
        if (object.lat != null)
            message.lat = Number(object.lat);
        if (object.lon != null)
            message.lon = Number(object.lon);
        if (object.velocity != null)
            message.velocity = object.velocity >>> 0;
        if (object.heading != null)
            message.heading = Number(object.heading);
        if (object.baroAltitude != null)
            message.baroAltitude = object.baroAltitude | 0;
        if (object.geoAltitude != null)
            message.geoAltitude = object.geoAltitude | 0;
        if (object.verticalRate != null)
            message.verticalRate = object.verticalRate | 0;
        if (object.squawk != null)
            message.squawk = object.squawk >>> 0;
        return message;
    };

    /**
     * Creates a plain object from an ObjectState message. Also converts values to other types if specified.
     * @function toObject
     * @memberof ObjectState
     * @static
     * @param {ObjectState} message ObjectState
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    ObjectState.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        let object = {};
        if (options.defaults) {
            object.reportedAt = 0;
            object.icao = 0;
            object.callsign = "";
            object.lat = 0;
            object.lon = 0;
            object.velocity = 0;
            object.heading = 0;
            object.baroAltitude = 0;
            object.geoAltitude = 0;
            object.verticalRate = 0;
            object.squawk = 0;
        }
        if (message.reportedAt != null && message.hasOwnProperty("reportedAt"))
            object.reportedAt = message.reportedAt;
        if (message.icao != null && message.hasOwnProperty("icao"))
            object.icao = message.icao;
        if (message.callsign != null && message.hasOwnProperty("callsign"))
            object.callsign = message.callsign;
        if (message.lat != null && message.hasOwnProperty("lat"))
            object.lat = options.json && !isFinite(message.lat) ? String(message.lat) : message.lat;
        if (message.lon != null && message.hasOwnProperty("lon"))
            object.lon = options.json && !isFinite(message.lon) ? String(message.lon) : message.lon;
        if (message.velocity != null && message.hasOwnProperty("velocity"))
            object.velocity = message.velocity;
        if (message.heading != null && message.hasOwnProperty("heading"))
            object.heading = options.json && !isFinite(message.heading) ? String(message.heading) : message.heading;
        if (message.baroAltitude != null && message.hasOwnProperty("baroAltitude"))
            object.baroAltitude = message.baroAltitude;
        if (message.geoAltitude != null && message.hasOwnProperty("geoAltitude"))
            object.geoAltitude = message.geoAltitude;
        if (message.verticalRate != null && message.hasOwnProperty("verticalRate"))
            object.verticalRate = message.verticalRate;
        if (message.squawk != null && message.hasOwnProperty("squawk"))
            object.squawk = message.squawk;
        return object;
    };

    /**
     * Converts this ObjectState to JSON.
     * @function toJSON
     * @memberof ObjectState
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    ObjectState.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return ObjectState;
})();

export const ObjectStatesMap = $root.ObjectStatesMap = (() => {

    /**
     * Properties of an ObjectStatesMap.
     * @exports IObjectStatesMap
     * @interface IObjectStatesMap
     * @property {Object.<string,IObjectState>|null} [states] ObjectStatesMap states
     */

    /**
     * Constructs a new ObjectStatesMap.
     * @exports ObjectStatesMap
     * @classdesc Represents an ObjectStatesMap.
     * @implements IObjectStatesMap
     * @constructor
     * @param {IObjectStatesMap=} [properties] Properties to set
     */
    function ObjectStatesMap(properties) {
        this.states = {};
        if (properties)
            for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * ObjectStatesMap states.
     * @member {Object.<string,IObjectState>} states
     * @memberof ObjectStatesMap
     * @instance
     */
    ObjectStatesMap.prototype.states = $util.emptyObject;

    /**
     * Creates a new ObjectStatesMap instance using the specified properties.
     * @function create
     * @memberof ObjectStatesMap
     * @static
     * @param {IObjectStatesMap=} [properties] Properties to set
     * @returns {ObjectStatesMap} ObjectStatesMap instance
     */
    ObjectStatesMap.create = function create(properties) {
        return new ObjectStatesMap(properties);
    };

    /**
     * Encodes the specified ObjectStatesMap message. Does not implicitly {@link ObjectStatesMap.verify|verify} messages.
     * @function encode
     * @memberof ObjectStatesMap
     * @static
     * @param {IObjectStatesMap} message ObjectStatesMap message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ObjectStatesMap.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.states != null && message.hasOwnProperty("states"))
            for (let keys = Object.keys(message.states), i = 0; i < keys.length; ++i) {
                writer.uint32(/* id 1, wireType 2 =*/10).fork().uint32(/* id 1, wireType 0 =*/8).uint32(keys[i]);
                $root.ObjectState.encode(message.states[keys[i]], writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim().ldelim();
            }
        return writer;
    };

    /**
     * Encodes the specified ObjectStatesMap message, length delimited. Does not implicitly {@link ObjectStatesMap.verify|verify} messages.
     * @function encodeDelimited
     * @memberof ObjectStatesMap
     * @static
     * @param {IObjectStatesMap} message ObjectStatesMap message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ObjectStatesMap.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes an ObjectStatesMap message from the specified reader or buffer.
     * @function decode
     * @memberof ObjectStatesMap
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {ObjectStatesMap} ObjectStatesMap
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ObjectStatesMap.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        let end = length === undefined ? reader.len : reader.pos + length, message = new $root.ObjectStatesMap(), key;
        while (reader.pos < end) {
            let tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                reader.skip().pos++;
                if (message.states === $util.emptyObject)
                    message.states = {};
                key = reader.uint32();
                reader.pos++;
                message.states[key] = $root.ObjectState.decode(reader, reader.uint32());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes an ObjectStatesMap message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof ObjectStatesMap
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {ObjectStatesMap} ObjectStatesMap
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ObjectStatesMap.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies an ObjectStatesMap message.
     * @function verify
     * @memberof ObjectStatesMap
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    ObjectStatesMap.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.states != null && message.hasOwnProperty("states")) {
            if (!$util.isObject(message.states))
                return "states: object expected";
            let key = Object.keys(message.states);
            for (let i = 0; i < key.length; ++i) {
                if (!$util.key32Re.test(key[i]))
                    return "states: integer key{k:uint32} expected";
                {
                    let error = $root.ObjectState.verify(message.states[key[i]]);
                    if (error)
                        return "states." + error;
                }
            }
        }
        return null;
    };

    /**
     * Creates an ObjectStatesMap message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof ObjectStatesMap
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {ObjectStatesMap} ObjectStatesMap
     */
    ObjectStatesMap.fromObject = function fromObject(object) {
        if (object instanceof $root.ObjectStatesMap)
            return object;
        let message = new $root.ObjectStatesMap();
        if (object.states) {
            if (typeof object.states !== "object")
                throw TypeError(".ObjectStatesMap.states: object expected");
            message.states = {};
            for (let keys = Object.keys(object.states), i = 0; i < keys.length; ++i) {
                if (typeof object.states[keys[i]] !== "object")
                    throw TypeError(".ObjectStatesMap.states: object expected");
                message.states[keys[i]] = $root.ObjectState.fromObject(object.states[keys[i]]);
            }
        }
        return message;
    };

    /**
     * Creates a plain object from an ObjectStatesMap message. Also converts values to other types if specified.
     * @function toObject
     * @memberof ObjectStatesMap
     * @static
     * @param {ObjectStatesMap} message ObjectStatesMap
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    ObjectStatesMap.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        let object = {};
        if (options.objects || options.defaults)
            object.states = {};
        let keys2;
        if (message.states && (keys2 = Object.keys(message.states)).length) {
            object.states = {};
            for (let j = 0; j < keys2.length; ++j)
                object.states[keys2[j]] = $root.ObjectState.toObject(message.states[keys2[j]], options);
        }
        return object;
    };

    /**
     * Converts this ObjectStatesMap to JSON.
     * @function toJSON
     * @memberof ObjectStatesMap
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    ObjectStatesMap.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return ObjectStatesMap;
})();

export const ObjectStates = $root.ObjectStates = (() => {

    /**
     * Properties of an ObjectStates.
     * @exports IObjectStates
     * @interface IObjectStates
     * @property {Array.<IObjectState>|null} [states] ObjectStates states
     */

    /**
     * Constructs a new ObjectStates.
     * @exports ObjectStates
     * @classdesc Represents an ObjectStates.
     * @implements IObjectStates
     * @constructor
     * @param {IObjectStates=} [properties] Properties to set
     */
    function ObjectStates(properties) {
        this.states = [];
        if (properties)
            for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * ObjectStates states.
     * @member {Array.<IObjectState>} states
     * @memberof ObjectStates
     * @instance
     */
    ObjectStates.prototype.states = $util.emptyArray;

    /**
     * Creates a new ObjectStates instance using the specified properties.
     * @function create
     * @memberof ObjectStates
     * @static
     * @param {IObjectStates=} [properties] Properties to set
     * @returns {ObjectStates} ObjectStates instance
     */
    ObjectStates.create = function create(properties) {
        return new ObjectStates(properties);
    };

    /**
     * Encodes the specified ObjectStates message. Does not implicitly {@link ObjectStates.verify|verify} messages.
     * @function encode
     * @memberof ObjectStates
     * @static
     * @param {IObjectStates} message ObjectStates message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ObjectStates.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.states != null && message.states.length)
            for (let i = 0; i < message.states.length; ++i)
                $root.ObjectState.encode(message.states[i], writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
        return writer;
    };

    /**
     * Encodes the specified ObjectStates message, length delimited. Does not implicitly {@link ObjectStates.verify|verify} messages.
     * @function encodeDelimited
     * @memberof ObjectStates
     * @static
     * @param {IObjectStates} message ObjectStates message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ObjectStates.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes an ObjectStates message from the specified reader or buffer.
     * @function decode
     * @memberof ObjectStates
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {ObjectStates} ObjectStates
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ObjectStates.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        let end = length === undefined ? reader.len : reader.pos + length, message = new $root.ObjectStates();
        while (reader.pos < end) {
            let tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.states && message.states.length))
                    message.states = [];
                message.states.push($root.ObjectState.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes an ObjectStates message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof ObjectStates
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {ObjectStates} ObjectStates
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ObjectStates.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies an ObjectStates message.
     * @function verify
     * @memberof ObjectStates
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    ObjectStates.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.states != null && message.hasOwnProperty("states")) {
            if (!Array.isArray(message.states))
                return "states: array expected";
            for (let i = 0; i < message.states.length; ++i) {
                let error = $root.ObjectState.verify(message.states[i]);
                if (error)
                    return "states." + error;
            }
        }
        return null;
    };

    /**
     * Creates an ObjectStates message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof ObjectStates
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {ObjectStates} ObjectStates
     */
    ObjectStates.fromObject = function fromObject(object) {
        if (object instanceof $root.ObjectStates)
            return object;
        let message = new $root.ObjectStates();
        if (object.states) {
            if (!Array.isArray(object.states))
                throw TypeError(".ObjectStates.states: array expected");
            message.states = [];
            for (let i = 0; i < object.states.length; ++i) {
                if (typeof object.states[i] !== "object")
                    throw TypeError(".ObjectStates.states: object expected");
                message.states[i] = $root.ObjectState.fromObject(object.states[i]);
            }
        }
        return message;
    };

    /**
     * Creates a plain object from an ObjectStates message. Also converts values to other types if specified.
     * @function toObject
     * @memberof ObjectStates
     * @static
     * @param {ObjectStates} message ObjectStates
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    ObjectStates.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        let object = {};
        if (options.arrays || options.defaults)
            object.states = [];
        if (message.states && message.states.length) {
            object.states = [];
            for (let j = 0; j < message.states.length; ++j)
                object.states[j] = $root.ObjectState.toObject(message.states[j], options);
        }
        return object;
    };

    /**
     * Converts this ObjectStates to JSON.
     * @function toJSON
     * @memberof ObjectStates
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    ObjectStates.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return ObjectStates;
})();

export const Picture = $root.Picture = (() => {

    /**
     * Properties of a Picture.
     * @exports IPicture
     * @interface IPicture
     * @property {string|null} [thumbnailUrl] Picture thumbnailUrl
     * @property {string|null} [picturePageUrl] Picture picturePageUrl
     * @property {string|null} [photographerFullname] Picture photographerFullname
     */

    /**
     * Constructs a new Picture.
     * @exports Picture
     * @classdesc Represents a Picture.
     * @implements IPicture
     * @constructor
     * @param {IPicture=} [properties] Properties to set
     */
    function Picture(properties) {
        if (properties)
            for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * Picture thumbnailUrl.
     * @member {string} thumbnailUrl
     * @memberof Picture
     * @instance
     */
    Picture.prototype.thumbnailUrl = "";

    /**
     * Picture picturePageUrl.
     * @member {string} picturePageUrl
     * @memberof Picture
     * @instance
     */
    Picture.prototype.picturePageUrl = "";

    /**
     * Picture photographerFullname.
     * @member {string} photographerFullname
     * @memberof Picture
     * @instance
     */
    Picture.prototype.photographerFullname = "";

    /**
     * Creates a new Picture instance using the specified properties.
     * @function create
     * @memberof Picture
     * @static
     * @param {IPicture=} [properties] Properties to set
     * @returns {Picture} Picture instance
     */
    Picture.create = function create(properties) {
        return new Picture(properties);
    };

    /**
     * Encodes the specified Picture message. Does not implicitly {@link Picture.verify|verify} messages.
     * @function encode
     * @memberof Picture
     * @static
     * @param {IPicture} message Picture message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Picture.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.thumbnailUrl != null && message.hasOwnProperty("thumbnailUrl"))
            writer.uint32(/* id 1, wireType 2 =*/10).string(message.thumbnailUrl);
        if (message.picturePageUrl != null && message.hasOwnProperty("picturePageUrl"))
            writer.uint32(/* id 2, wireType 2 =*/18).string(message.picturePageUrl);
        if (message.photographerFullname != null && message.hasOwnProperty("photographerFullname"))
            writer.uint32(/* id 3, wireType 2 =*/26).string(message.photographerFullname);
        return writer;
    };

    /**
     * Encodes the specified Picture message, length delimited. Does not implicitly {@link Picture.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Picture
     * @static
     * @param {IPicture} message Picture message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Picture.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a Picture message from the specified reader or buffer.
     * @function decode
     * @memberof Picture
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Picture} Picture
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Picture.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Picture();
        while (reader.pos < end) {
            let tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.thumbnailUrl = reader.string();
                break;
            case 2:
                message.picturePageUrl = reader.string();
                break;
            case 3:
                message.photographerFullname = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a Picture message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Picture
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Picture} Picture
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Picture.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a Picture message.
     * @function verify
     * @memberof Picture
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    Picture.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.thumbnailUrl != null && message.hasOwnProperty("thumbnailUrl"))
            if (!$util.isString(message.thumbnailUrl))
                return "thumbnailUrl: string expected";
        if (message.picturePageUrl != null && message.hasOwnProperty("picturePageUrl"))
            if (!$util.isString(message.picturePageUrl))
                return "picturePageUrl: string expected";
        if (message.photographerFullname != null && message.hasOwnProperty("photographerFullname"))
            if (!$util.isString(message.photographerFullname))
                return "photographerFullname: string expected";
        return null;
    };

    /**
     * Creates a Picture message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Picture
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Picture} Picture
     */
    Picture.fromObject = function fromObject(object) {
        if (object instanceof $root.Picture)
            return object;
        let message = new $root.Picture();
        if (object.thumbnailUrl != null)
            message.thumbnailUrl = String(object.thumbnailUrl);
        if (object.picturePageUrl != null)
            message.picturePageUrl = String(object.picturePageUrl);
        if (object.photographerFullname != null)
            message.photographerFullname = String(object.photographerFullname);
        return message;
    };

    /**
     * Creates a plain object from a Picture message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Picture
     * @static
     * @param {Picture} message Picture
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Picture.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        let object = {};
        if (options.defaults) {
            object.thumbnailUrl = "";
            object.picturePageUrl = "";
            object.photographerFullname = "";
        }
        if (message.thumbnailUrl != null && message.hasOwnProperty("thumbnailUrl"))
            object.thumbnailUrl = message.thumbnailUrl;
        if (message.picturePageUrl != null && message.hasOwnProperty("picturePageUrl"))
            object.picturePageUrl = message.picturePageUrl;
        if (message.photographerFullname != null && message.hasOwnProperty("photographerFullname"))
            object.photographerFullname = message.photographerFullname;
        return object;
    };

    /**
     * Converts this Picture to JSON.
     * @function toJSON
     * @memberof Picture
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    Picture.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Picture;
})();

export const FlightHistory = $root.FlightHistory = (() => {

    /**
     * Properties of a FlightHistory.
     * @exports IFlightHistory
     * @interface IFlightHistory
     * @property {Array.<IObjectState>|null} [states] FlightHistory states
     * @property {Array.<IPicture>|null} [pictures] FlightHistory pictures
     */

    /**
     * Constructs a new FlightHistory.
     * @exports FlightHistory
     * @classdesc Represents a FlightHistory.
     * @implements IFlightHistory
     * @constructor
     * @param {IFlightHistory=} [properties] Properties to set
     */
    function FlightHistory(properties) {
        this.states = [];
        this.pictures = [];
        if (properties)
            for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * FlightHistory states.
     * @member {Array.<IObjectState>} states
     * @memberof FlightHistory
     * @instance
     */
    FlightHistory.prototype.states = $util.emptyArray;

    /**
     * FlightHistory pictures.
     * @member {Array.<IPicture>} pictures
     * @memberof FlightHistory
     * @instance
     */
    FlightHistory.prototype.pictures = $util.emptyArray;

    /**
     * Creates a new FlightHistory instance using the specified properties.
     * @function create
     * @memberof FlightHistory
     * @static
     * @param {IFlightHistory=} [properties] Properties to set
     * @returns {FlightHistory} FlightHistory instance
     */
    FlightHistory.create = function create(properties) {
        return new FlightHistory(properties);
    };

    /**
     * Encodes the specified FlightHistory message. Does not implicitly {@link FlightHistory.verify|verify} messages.
     * @function encode
     * @memberof FlightHistory
     * @static
     * @param {IFlightHistory} message FlightHistory message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    FlightHistory.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.states != null && message.states.length)
            for (let i = 0; i < message.states.length; ++i)
                $root.ObjectState.encode(message.states[i], writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
        if (message.pictures != null && message.pictures.length)
            for (let i = 0; i < message.pictures.length; ++i)
                $root.Picture.encode(message.pictures[i], writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
        return writer;
    };

    /**
     * Encodes the specified FlightHistory message, length delimited. Does not implicitly {@link FlightHistory.verify|verify} messages.
     * @function encodeDelimited
     * @memberof FlightHistory
     * @static
     * @param {IFlightHistory} message FlightHistory message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    FlightHistory.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a FlightHistory message from the specified reader or buffer.
     * @function decode
     * @memberof FlightHistory
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {FlightHistory} FlightHistory
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    FlightHistory.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        let end = length === undefined ? reader.len : reader.pos + length, message = new $root.FlightHistory();
        while (reader.pos < end) {
            let tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.states && message.states.length))
                    message.states = [];
                message.states.push($root.ObjectState.decode(reader, reader.uint32()));
                break;
            case 2:
                if (!(message.pictures && message.pictures.length))
                    message.pictures = [];
                message.pictures.push($root.Picture.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a FlightHistory message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof FlightHistory
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {FlightHistory} FlightHistory
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    FlightHistory.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a FlightHistory message.
     * @function verify
     * @memberof FlightHistory
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    FlightHistory.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.states != null && message.hasOwnProperty("states")) {
            if (!Array.isArray(message.states))
                return "states: array expected";
            for (let i = 0; i < message.states.length; ++i) {
                let error = $root.ObjectState.verify(message.states[i]);
                if (error)
                    return "states." + error;
            }
        }
        if (message.pictures != null && message.hasOwnProperty("pictures")) {
            if (!Array.isArray(message.pictures))
                return "pictures: array expected";
            for (let i = 0; i < message.pictures.length; ++i) {
                let error = $root.Picture.verify(message.pictures[i]);
                if (error)
                    return "pictures." + error;
            }
        }
        return null;
    };

    /**
     * Creates a FlightHistory message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof FlightHistory
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {FlightHistory} FlightHistory
     */
    FlightHistory.fromObject = function fromObject(object) {
        if (object instanceof $root.FlightHistory)
            return object;
        let message = new $root.FlightHistory();
        if (object.states) {
            if (!Array.isArray(object.states))
                throw TypeError(".FlightHistory.states: array expected");
            message.states = [];
            for (let i = 0; i < object.states.length; ++i) {
                if (typeof object.states[i] !== "object")
                    throw TypeError(".FlightHistory.states: object expected");
                message.states[i] = $root.ObjectState.fromObject(object.states[i]);
            }
        }
        if (object.pictures) {
            if (!Array.isArray(object.pictures))
                throw TypeError(".FlightHistory.pictures: array expected");
            message.pictures = [];
            for (let i = 0; i < object.pictures.length; ++i) {
                if (typeof object.pictures[i] !== "object")
                    throw TypeError(".FlightHistory.pictures: object expected");
                message.pictures[i] = $root.Picture.fromObject(object.pictures[i]);
            }
        }
        return message;
    };

    /**
     * Creates a plain object from a FlightHistory message. Also converts values to other types if specified.
     * @function toObject
     * @memberof FlightHistory
     * @static
     * @param {FlightHistory} message FlightHistory
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    FlightHistory.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        let object = {};
        if (options.arrays || options.defaults) {
            object.states = [];
            object.pictures = [];
        }
        if (message.states && message.states.length) {
            object.states = [];
            for (let j = 0; j < message.states.length; ++j)
                object.states[j] = $root.ObjectState.toObject(message.states[j], options);
        }
        if (message.pictures && message.pictures.length) {
            object.pictures = [];
            for (let j = 0; j < message.pictures.length; ++j)
                object.pictures[j] = $root.Picture.toObject(message.pictures[j], options);
        }
        return object;
    };

    /**
     * Converts this FlightHistory to JSON.
     * @function toJSON
     * @memberof FlightHistory
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    FlightHistory.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return FlightHistory;
})();

export { $root as default };
