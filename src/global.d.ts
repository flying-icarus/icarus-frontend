import Konva from 'konva'
import L from 'leaflet'

// Complementary type declarations for third party modules.
declare module 'konva' {
    interface Stage {
        size(a: any): any
    }
}

declare module 'leaflet' {
    interface Map {
        // TODO: Don't rely on private methods.
        _latLngBoundsToNewLayerBounds(a: any, b: any, c: any): any
    }
}
