import { Action } from 'redux'
import * as models from '../models'

export enum ACTION_TYPES {
    // Map
    REQUEST_OBJECT_TRAIL = 'REQUEST_OBJECT_TRAIL',
    SET_OBJECT_TRAIL = 'SET_OBJECT_TRAIL',
    REQUEST_STATES = 'REQUEST_STATES',
    SET_OBJECT_STATES = 'SET_OBJECT_STATES',
    PLAYBACK_ENABLE = 'PLAYBACK_ENABLE',
    PLAYBACK_DISABLE = 'PLAYBACK_DISABLE',
    PLAYBACK_SET_SPEED = 'PLAYBACK_SET_SPEED',
    MAP_OBJECT_SELECTED = 'MAP_OBJECT_SELECTED',
    MAP_OBJECT_DESELECTED = 'MAP_OBJECT_DESELECTED',
    MAP_OBJECT_MOUSEENTER = 'MAP_OBJECT_MOUSEENTER',
    MAP_OBJECT_MOUSELEAVE = 'MAP_OBJECT_MOUSELEAVE',
    SET_VIEW_BBOX = 'SET_VIEW_BBOX',
    SET_ROOT_ERROR = 'SET_ROOT_ERROR',

    // Search
    REQUEST_OBJECT_HISTORY = 'REQUEST_OBJECT_HISTORY',
    SET_OBJECT_HISTORY = 'SET_OBJECT_HISTORY',
}

export interface ISetObjectTrail extends Action<string> {
    history: models.FlightHistory,
}

export interface IRequestStates extends Action<string> {
    timestamp: number,
    bbox: models.BoundingBox,
}

export interface ISetRootError extends Action<string> {
    error: Error,
}


export interface IMapObjectSelected extends Action<string> {
    icao: number,
}

export interface IRequestObjectTrail extends Action<string> {
    icao: number,
}

export interface ISetViewBBox extends Action<string> {
    bbox: models.BoundingBox,
}

export interface ISetNextRefreshTimestamp extends Action<string> {
    timestamp: number,
}

export interface IPlaybackEnable extends Action<string> {
    timestamp: number,
    speed: number,
}

export function playbackEnable(timestamp: number, speed: number): IPlaybackEnable {
    return {
        type: ACTION_TYPES.PLAYBACK_ENABLE,
        timestamp,
        speed,
    }
}

export interface IPlaybackDisable extends Action<string> { }
export function playbackDisable(): IPlaybackDisable {
    return {
        type: ACTION_TYPES.PLAYBACK_DISABLE,
    }
}

export interface IPlaybackSetSpeed extends Action<string> {
    speed: number,
}

export function setPlaybackSpeed(speed: number): IPlaybackSetSpeed {
    return {
        type: ACTION_TYPES.PLAYBACK_SET_SPEED,
        speed,
    }
}

export function setViewBBox(bbox: models.BoundingBox) {
    window.localStorage.setItem('view-bbox', bbox.join(","))
    return {
        type: ACTION_TYPES.SET_VIEW_BBOX,
        bbox,
    }
}

export const setStates = (states: models.ObjectStatesMap) => ({
    type: ACTION_TYPES.SET_OBJECT_STATES,
    states,
})

export const setRootError = (error: models.RootError) => ({
    type: ACTION_TYPES.SET_ROOT_ERROR,
    message: error.message,
})

export function setSelectedObject(icao: number): IMapObjectSelected {
    return {
        type: ACTION_TYPES.MAP_OBJECT_SELECTED,
        icao,
    }
}

export function setObjectTrail(history: models.FlightHistory): ISetObjectTrail {
    return {
        type: ACTION_TYPES.SET_OBJECT_TRAIL,
        history: history,
    }
}

export function requestStates(timestamp: number, bbox: models.BoundingBox): IRequestStates {
    return {
        type: ACTION_TYPES.REQUEST_STATES,
        timestamp,
        bbox,
    }
}

export interface ISetMouseOverObject extends Action<string> {
    icao: number
}

export function setMouseOverObject(icao: number): ISetMouseOverObject {
    return {
        type: ACTION_TYPES.MAP_OBJECT_MOUSEENTER,
        icao,
    }
}

export function unsetMouseOverObject(): Action<string> {
    return {
        type: ACTION_TYPES.MAP_OBJECT_MOUSELEAVE,
    }
}

export interface IRequestObjectHistoryIn {
    icao?: number,
    registration?: string,
    callsign?: string,
}

export interface IRequestObjectHistoryOut extends Action<string> {
    filter: IRequestObjectHistoryIn,
}

export function requestObjectHistory(filter: IRequestObjectHistoryIn): IRequestObjectHistoryOut {
    if (
        (filter.icao === undefined) &&
        (filter.registration === undefined) &&
        (filter.callsign === undefined)
    ) {
        throw new Error("need to supply at least one filter")
    }

    return {
        type: ACTION_TYPES.REQUEST_OBJECT_HISTORY,
        filter,
    }
}

export interface ISetObjectHistory extends Action<string> {
    history: models.ObjectHistory,
}

export function setObjectHistory(history: models.ObjectHistory): ISetObjectHistory {
    return {
        type: ACTION_TYPES.SET_OBJECT_HISTORY,
        history: history,
    }
}
