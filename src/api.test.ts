// TODO: Fix the only tests we had.

import fetchMock from 'fetch-mock'
import * as api from './api'


const SAMPLE_STATES_RESPONSE: api.IObjectStatesResponse = {
    "states": {
        "655429": [
            655429,
            "DAH4633",
            34.695499420166016,
            22.556400299072266,
            11437.6201171875,
            11582.400390625,
            232.66000366210938,
            281.6099853515625,
            0.33000001311302185,
            2451,
            null,
            1549465002.0
        ],
        "131186": [
            131186,
            "RAM277",
            36.0077018737793,
            23.716299057006836,
            10759.4404296875,
            10965.1796875,
            233.0500030517578,
            269.75,
            -0.33000001311302185,
            5636,
            null,
            1549465004.0
        ]
    }
}

it('fetches the states', async () => {
    fetchMock.get('path:/states', SAMPLE_STATES_RESPONSE)
    let states = await api.getObjectStates(1, [1, 1, 1, 1])
    let icaoStr = "655429"
    let stateTuple = SAMPLE_STATES_RESPONSE.states[icaoStr]
    let stateIcaoInt = stateTuple[0]
    expect(states).toHaveProperty(icaoStr)
    expect(states[stateIcaoInt]).toEqual({
        "icao": stateTuple[0],
        "callsign": stateTuple[1],
        "lat": stateTuple[2],
        "lon": stateTuple[3],
        "geoAltitude": stateTuple[4],
        "baroAltitude": stateTuple[5],
        "velocity": stateTuple[6],
        "heading": stateTuple[7],
        "verticalRate": stateTuple[8],
        "squawk": stateTuple[9],
        "onGround": stateTuple[10],
        "reportedAt": stateTuple[11],
    })
})
